#!/usr/bin/ruby1.8
# -*- coding: utf-8 -*-
require 'haml'
require 'singleton'

$srcdir = File.dirname(__FILE__)

class Seco3
  include Singleton
  Contents = {
    'autores' => 'Acerca de los autores',
    'index' => '',
    'parte1' => 'Parte I: Software y medios libres',
    'parte2' => 'Parte II: Comunidades',
    'parte3' => 'Parte III: Sociedad',
    'apendices' => 'Apéndices',
    'videoconferencias' => 'Videoconferencias',
    'video_presentacion' => 'Video: Presentación',
    'video_presen_iiec' => 'Video: Presentación en el Instituto de Investigaciones Económicas',
    'video_cap_1' => 'Video: Software libre y construcción democrática de la sociedad',
    'video_cap_2' => 'Video: Esquemas permisivos de licenciamiento en la creación artística',
    'video_cap_3' => 'Video: Fundamentos teóricos y ubicación histórica de la economía y sociedad del conocimiento',
    'video_cap_4_y_ap1' => 'Video: Educación y Software Libre / Factores de motivación y elementos de reconocimiento',
    'video_cap_5' => 'Video: La construcción colaborativa del conocimiento visto desde la óptica de las comunidades de software libre',
    'video_cap_6' => 'Video: Política 2.0',
    'video_cap_7' => 'Video: Analfabetización informática o ¿por qué los programas privativos fomentan la analfabetización?',
    'video_apend_4' => 'Video: Traducción de herramientas para revaloración y rescate de la lectura tradicional',
    'res_cap_1' => 'Reseñas: Software Libre y Construcción Democrática de la Sociedad',
    'res_cap_2' => 'Reseñas: Esquemas permisivos de licenciamiento en la creación artística',
    'res_cap_3' => 'Reseñas: Fundamentos teóricos y ubicación histórica de la economía y sociedad del conocimiento',
    'res_cap_5' => 'Reseñas: La construcción colaborativa del conocimiento visto desde la óptica de las comunidades de software libre',
    'res_apend_4' => 'Reseñas: Equipos de desarrollo y traducción de herramientas para revaloración y rescate de la cultura',
    'compra' => '¿Cómo comprar el libro?',
    'presentacion' => 'Presentación',
    'portada' => 'La portada',
    'actividades' => 'Actividades y reseñas',
    'pres_cordoba' => 'Presentación en Córdoba, Argentina',
    'legal' => 'Información legal'
  }

  def set_to(what)
    return false unless Contents.keys.include? what
    @current = what
  end

  def full_title
    base = 'Seminario «Construcción Colaborativa del Conocimiento»'
    return base unless title
    '%s ‣ %s' % [base, title]
  end

  def title
    return nil unless @current and Contents[@current]
    Contents[@current]
  end

  def video(file)
    @videofile = file
    Haml::Engine.new(readhaml('applet_video')).render
  end

  def videofile
    @videofile
  end

  def body
    Haml::Engine.new(readhaml(@current)).render
  end

  private
  def readhaml(file)
    File.open(File.join($srcdir, '%s.haml' % file)).read
  end
end

class Hamltmpl
  def initialize(tmpl)
    # We expect the HAML files to be in the same directory as this
    # executable
    tmplfile = File.join($srcdir, tmpl)
puts "Reading template: %s"%tmplfile
    @engine = Haml::Engine.new(File.open(tmplfile).read)
  end

  def render
    @engine.render
  end
end

haml = Hamltmpl.new('modelo.haml')
Seco3::Contents.keys.sort.each do |file|
  puts '=== %s' %file
  Seco3.instance.set_to(file)
  File.open(File.join($srcdir, '../dest/%s.html' % file), 'w') {|f| f.puts haml.render}
end
