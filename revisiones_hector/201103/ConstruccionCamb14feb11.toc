\select@language {spanish}
\contentsline {chapter}{Introducci\'on}{9}
\contentsline {section}{El proyecto}{10}
\contentsline {subsection}{Manejo de referencias}{12}
\contentsline {section}{Implementaci\'on t\'ecnica}{14}
\contentsline {section}{Acerca de los autores}{18}
\contentsline {subsection}{Beatriz Busaniche (Argentina)}{18}
\contentsline {subsection}{H\'ector Colina (Venezuela)}{18}
\contentsline {subsection}{Carolina Flores (Costa Rica)}{19}
\contentsline {subsection}{Antonio Galindo (M\'exico)}{19}
\contentsline {subsection}{Alejandro Miranda (M\'exico)}{19}
\contentsline {subsection}{Sergio Ord\'o\~nez (M\'exico)}{20}
\contentsline {subsection}{Lila Pagola (Argentina)}{20}
\contentsline {subsection}{Marko Txopitea (Euskal Herria)}{21}
\contentsline {subsection}{\'Erika Valverde (Costa Rica)}{21}
\contentsline {subsection}{Gunnar Wolf (M\'exico)}{22}
\contentsline {part}{I\hspace {1em}Software y medios libres}{23}
\contentsline {chapter}{Software libre y construcci\'on democr\'atica de la sociedad}{25}
\contentsline {section}{Introducci\'on}{26}
\contentsline {subsection}{La historia del conocimiento}{26}
\contentsline {subsection}{El c\'omputo como expresi\'on del conocimiento}{30}
\contentsline {subsection}{\IeC {\textquestiondown }Qu\'e es una sociedad democr\'atica?}{32}
\contentsline {subsection}{Del software al conocimiento}{35}
\contentsline {section}{El software libre en tanto movimiento social}{38}
\contentsline {section}{Conclusiones}{43}
\contentsline {chapter}{Esquemas permisivos de licenciamiento en la creaci\'on art\IeC {\'\i }stica}{45}
\contentsline {section}{Introducci\'on}{45}
\contentsline {section}{Posiciones respecto de los derechos de autor y de copia en el arte}{47}
\contentsline {subsection}{Actitud copyleft en el arte}{50}
\contentsline {subsection}{Reacciones desde la industria cultural y las gestoras de derechos de autor}{51}
\contentsline {section}{Cr\IeC {\'\i }tica a la noci\'on de autor: antecedentes en la historia del arte}{52}
\contentsline {section}{Efecto copyleft avant la lettre: c\'omo explicar el copyleft cuando todos lo practicamos}{53}
\contentsline {subsection}{1984, m\'as ac\'a de Orwell: Stallman y Jobs}{54}
\contentsline {subsection}{Los problemas de la naturalizaci\'on: el doble est\'andar}{58}
\contentsline {subsection}{La extra\~neza en la discusi\'on sobre el copyleft}{59}
\contentsline {subsection}{Esquema de colaboraci\'on}{61}
\contentsline {section}{Convergencia de ideas en los modelos alternativos de circulaci\'on que propone el movimiento de software libre desde otras \'areas de la producci\'on cultural}{63}
\contentsline {subsection}{Potenciales de convergencia en una \'epoca de redefiniciones}{64}
\contentsline {paragraph}{$<$T4$>$La afinidad ideol\'ogica de las propuestas: develar el desconocimiento mutuo}{64}
\contentsline {paragraph}{$<$T4$>$Propuestas y proyecciones}{67}
\contentsline {paragraph}{$<$T4$>$Sobre las distancias autor-receptor}{67}
\contentsline {subsection}{Aspectos en debate sobre las licencias permisivas en el arte}{68}
\contentsline {paragraph}{$<$T4$>$Diversidad y calidad de las obras de libre circulaci\'on}{68}
\contentsline {paragraph}{$<$T4$>$Puntos problem\'aticos para los autores en las licencias permisivas}{69}
\contentsline {paragraph}{$<$T4$>$Desconocimiento del copyright y, por extensi\'on, errores de interpretaci\'on sobre el copyleft}{69}
\contentsline {paragraph}{$<$T4$>$Desinter\'es por la mara\~na t\'ecnico-legal}{70}
\contentsline {paragraph}{$<$T4$>$Usos comerciales}{71}
\contentsline {paragraph}{$<$T4$>$Obras derivadas}{72}
\contentsline {paragraph}{$<$T4$>$La obra derivada sin control: o la preocupaci\'on por evitar ``ciertas'' derivaciones}{73}
\contentsline {chapter}{Fundamentos te\'oricos y ubicaci\'on hist\'orica de la econom\IeC {\'\i }a y sociedad del conocimiento}{77}
\contentsline {section}{Introducci\'on}{77}
\contentsline {section}{Econom\IeC {\'\i }a y sociedad del conocimiento}{79}
\contentsline {section}{V\IeC {\'\i }as de desarrollo de los embriones de la sociedad del conocimiento}{84}
\contentsline {section}{\IeC {\textquestiondown }Hacia una sociedad del conocimiento inclusiva y participativa?}{90}
\contentsline {section}{Referencias}{98}
\contentsline {part}{II\hspace {1em}Comunidades}{105}
\contentsline {chapter}{Factores de motivaci\'on y elementos de reconocimiento}{107}
\contentsline {section}{Factores de motivaci\'on}{108}
\contentsline {subsection}{La programaci\'on como un acto de belleza o de arte}{109}
\contentsline {subsection}{Programar por diversi\'on}{111}
\contentsline {subsection}{Semillero de interacciones sociales}{112}
\contentsline {subsection}{Enfatizando en las ventajas del modelo distribuido, descentralizado de desarrollo}{114}
\contentsline {subsection}{La econom\IeC {\'\i }a del regalo}{119}
\contentsline {subsection}{Impulso \'etico-ideol\'ogico}{120}
\contentsline {subsection}{Incidencia del proyecto en un \'area de inter\'es profesional}{122}
\contentsline {section}{Elementos de identificaci\'on con la subcultura hacker}{123}
\contentsline {section}{Paralelos en otros grupos creadores}{126}
\contentsline {subsection}{Paralelos con la comunidad cient\IeC {\'\i }fica}{127}
\contentsline {subsection}{Comunidades de creaci\'on art\IeC {\'\i }stica}{128}
\contentsline {section}{Diferentes formas y niveles de participaci\'on}{130}
\contentsline {section}{Jerarquizaci\'on de los individuos en la sociedad}{134}
\contentsline {subsection}{Sistemas de redes sociales}{134}
\contentsline {section}{Conclusiones}{139}
\contentsline {chapter}{La construcci\'on colaborativa del conocimiento desde la \'optica de las comunidades de {\it software} libre}{141}
\contentsline {section}{Introducci\'on}{142}
\contentsline {section}{Tecnolog\IeC {\'\i }a y comunidades de software libre}{143}
\contentsline {subsection}{Repensar la tecnolog\IeC {\'\i }a}{143}
\contentsline {section}{El conocimiento es un hecho social}{145}
\contentsline {section}{Comunidades de software libre}{145}
\contentsline {section}{Elementos para entender las comunidades de software libre}{146}
\contentsline {section}{Referencias}{147}
\contentsline {part}{III\hspace {1em}Sociedad}{151}
\contentsline {chapter}{Pol\IeC {\'\i }tica 2.0}{153}
\contentsline {section}{Introducci\'on}{153}
\contentsline {section}{Una definici\'on de pol\IeC {\'\i }tica 2.0}{157}
\contentsline {section}{Algunas iniciativas locales}{160}
\contentsline {subsection}{Politika 2.0}{160}
\contentsline {subsection}{Colabora en nuestras ciudades}{160}
\contentsline {subsection}{Ciudadanos 2010}{161}
\contentsline {subsection}{Parlio}{161}
\contentsline {subsection}{Ezker Batua Berdeak}{162}
\contentsline {subsection}{Neskateka}{162}
\contentsline {subsection}{arreglaMicalle}{163}
\contentsline {subsection}{Queremos software libre}{163}
\contentsline {subsection}{Sindominio}{164}
\contentsline {subsection}{Indymedia}{164}
\contentsline {subsection}{Hacktivistas}{165}
\contentsline {subsection}{Partido Pirata}{165}
\contentsline {subsection}{Partido de Internet}{166}
\contentsline {subsection}{Irekia}{168}
\contentsline {section}{Conclusiones}{168}
\contentsline {subsection}{Caracter\IeC {\'\i }sticas comunes}{168}
\contentsline {subsection}{Tipos de grupos}{169}
\contentsline {chapter}{Analfabetizaci\'on inform\'atica o \IeC {\textquestiondown }por qu\'e los programas privativos fomentan la analfabetizaci\'on?}{173}
\contentsline {section}{Introducci\'on}{173}
\contentsline {section}{\IeC {\textquestiondown }Qu\'e entendemos por alfabetizaci\'on?}{175}
\contentsline {section}{Contenido y estructura}{177}
\contentsline {section}{El futuro en el jard\IeC {\'\i }n de infantes}{178}
\contentsline {section}{Perspectivas y propuestas}{181}
\contentsline {chapter}{Construyendo metodolog\IeC {\'\i }as para la infoinclusi\'on}{185}
\contentsline {section}{Punto de encuentro: la infoinclusi\'on}{186}
\contentsline {section}{La historia personal y organizativa}{193}
\contentsline {section}{El enfoque de g\'enero}{196}
\contentsline {section}{Experiencias}{199}
\contentsline {subsection}{Usos estrat\'egicos de internet con campesinos}{199}
\contentsline {subsection}{Manejo seguro de la informaci\'on con dos organizaciones que trabajan en derechos humanos}{205}
\contentsline {section}{Notas finales}{211}
\contentsline {section}{Referencias}{212}
\contentsline {part}{IV\hspace {1em}Ap\'endices}{217}
\contentsline {chapter}{Ap\'endice A. Educaci\'on y {\it software} libre}{219}
\contentsline {section}{Eje tecnol\'ogico}{222}
\contentsline {subsection}{\IeC {\textquestiondown }Software libre en la educaci\'on o educaci\'on con software libre?}{222}
\contentsline {subsection}{Argumentos del eje tecnol\'ogico, de la ingenuidad educativa}{226}
\contentsline {section}{Eje \'etico y cognitivo, el software libre como herramienta del pensamiento humano}{228}
\contentsline {subsection}{De vuelta a lo importante, la educaci\'on}{228}
\contentsline {subsection}{Educaci\'on camino a los bienes comunes}{229}
\contentsline {subsection}{De la cultura libre a la educaci\'on libre}{235}
\contentsline {subsection}{Bienes culturales libres: dibujando el futuro}{239}
\contentsline {chapter}{Ap\'endice B. Sugar}{241}
\contentsline {section}{Construccionismo: un enfoque educativo centrado en el aprendiz}{242}
\contentsline {section}{Los ni\~nos no son oficinistas}{245}
\contentsline {section}{Actividades para construir y compartir}{247}
\contentsline {section}{Abramos la educaci\'on}{251}
\contentsline {section}{Evidencia}{254}
\contentsline {section}{Sugar y su implementaci\'on en escuelas}{258}
\contentsline {section}{Cierre}{260}
\contentsline {chapter}{Ap\'endice C. Voto electr\'onico: \IeC {\textquestiondown }Qui\'en tiene realmente la decisi\'on?}{263}
\contentsline {section}{Disminuci\'on de costos}{265}
\contentsline {section}{Agilidad en la obtenci\'on de resultados}{268}
\contentsline {section}{Confiabilidad de los actores}{270}
\contentsline {section}{Votos blancos y nulos: expresi\'on leg\IeC {\'\i }tima del ciudadano}{274}
\contentsline {section}{Experiencias internacionales}{276}
\contentsline {chapter}{Ap\'endice D. Traducci\'on de herramientas para revaloraci\'on y rescate de la lectura tradicional}{281}
\contentsline {section}{Exposici\'on}{281}
\contentsline {section}{Sesi\'on de preguntas y respuestas}{291}
\contentsline {section}{Referencias}{301}
