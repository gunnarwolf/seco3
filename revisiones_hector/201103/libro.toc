\select@language {spanish}
\contentsline {chapter}{Introducci\'on}{7}
\contentsline {section}{El proyecto}{7}
\contentsline {section}{Forma de trabajo}{8}
\contentsline {subsection}{Esquema de colaboraci\'on}{8}
\contentsline {subsection}{Manejo de referencias}{9}
\contentsline {section}{Implementaci\'on t\'ecnica}{10}
\contentsline {section}{Acerca de los autores}{12}
\contentsline {subsection}{Beatriz Busaniche (Argentina)}{12}
\contentsline {subsection}{H\'ector Colina (Venezuela)}{13}
\contentsline {subsection}{Carolina Flores (Costa Rica)}{13}
\contentsline {subsection}{Antonio Galindo (M\'exico)}{13}
\contentsline {subsection}{Alejandro Miranda (M\'exico)}{14}
\contentsline {subsection}{Sergio Ord\'o\~nez (M\'exico)}{14}
\contentsline {subsection}{Lila Pagola (Argentina)}{14}
\contentsline {subsection}{Marko Txopitea (Euskal Herria)}{14}
\contentsline {subsection}{\'Erika Valverde (Costa Rica)}{15}
\contentsline {subsection}{Gunnar Wolf (M\'exico)}{15}
\contentsline {part}{I\hspace {1em}Software y medios libres}{17}
\contentsline {chapter}{Software libre y construcci\'on democr\'atica de la sociedad}{19}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{19}
\contentsline {paragraph}{$<$T3$>$La historia del conocimiento}{20}
\contentsline {paragraph}{$<$T3$>$El c\'omputo como expresi\'on del conocimiento}{22}
\contentsline {paragraph}{$<$T3$>$\IeC {\textquestiondown }Qu\'e es una sociedad democr\'atica?}{23}
\contentsline {paragraph}{$<$T3$>$Del software al conocimiento}{25}
\contentsline {subsection}{\numberline {}$<$T2$>$El software libre en tanto movimiento social}{27}
\contentsline {subsection}{\numberline {}$<$T2$>$Conclusiones}{30}
\contentsline {chapter}{$<$T1$>$Esquemas permisivos de licenciamiento en la creaci\'on art\IeC {\'\i }stica}{31}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{31}
\contentsline {subsection}{\numberline {}$<$T2$>$Posiciones respecto de los derechos de autor y de copia en el arte}{32}
\contentsline {paragraph}{$<$T3$>$Actitud copyleft en el arte}{34}
\contentsline {paragraph}{$<$T3$>$Reacciones desde la industria cultural y las gestoras de derechos de autor}{34}
\contentsline {subsection}{\numberline {}$<$T2$>$Cr\IeC {\'\i }tica a la noci\'on de autor: antecedentes en la historia del arte}{35}
\contentsline {subsection}{\numberline {}$<$T2$>$Efecto copyleft avant la lettre: c\'omo explicar el copyleft cuando todos lo practicamos}{36}
\contentsline {paragraph}{$<$T3$>$1984, m\'as ac\'a de Orwell: Stallman y Jobs}{36}
\contentsline {paragraph}{$<$T3$>$Los problemas de la naturalizaci\'on: el doble est\'andar}{39}
\contentsline {paragraph}{$<$T3$>$La extra\~neza en la discusi\'on sobre el copyleft}{39}
\contentsline {paragraph}{$<$T3$>$Efecto performativo del copyleft}{40}
\contentsline {subsection}{\numberline {}$<$T2$>$Convergencia de ideas en los modelos alternativos de circulaci\'on que propone el movimiento de software libre desde otras \'areas de la producci\'on cultural}{42}
\contentsline {paragraph}{$<$T3$>$Potenciales de convergencia en una \'epoca de redefiniciones}{42}
\contentsline {paragraph}{$<$T4$>$La afinidad ideol\'ogica de las propuestas: develar el desconocimiento mutuo}{43}
\contentsline {paragraph}{$<$T4$>$Propuestas y proyecciones}{44}
\contentsline {paragraph}{$<$T4$>$Sobre las distancias autor-receptor}{44}
\contentsline {paragraph}{$<$T3$>$Aspectos en debate sobre las licencias permisivas en el arte}{45}
\contentsline {paragraph}{$<$T4$>$Diversidad y calidad de las obras de libre circulaci\'on}{45}
\contentsline {paragraph}{$<$T4$>$Puntos problem\'aticos para los autores en las licencias permisivas}{45}
\contentsline {paragraph}{$<$T4$>$Desconocimiento del copyright y, por extensi\'on, errores de interpretaci\'on sobre el copyleft}{45}
\contentsline {paragraph}{$<$T4$>$Desinter\'es por la mara\~na t\'ecnico-legal}{46}
\contentsline {paragraph}{$<$T4$>$Usos comerciales}{46}
\contentsline {paragraph}{$<$T4$>$Obras derivadas}{47}
\contentsline {paragraph}{$<$T4$>$La obra derivada sin control: o la preocupaci\'on por evitar ``ciertas'' derivaciones}{48}
\contentsline {chapter}{$<$T1$>$Fundamentos te\'oricos y ubicaci\'on hist\'orica de la econom\IeC {\'\i }a y sociedad del conocimiento}{51}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{51}
\contentsline {subsection}{\numberline {}$<$T2$>$Econom\IeC {\'\i }a y sociedad del conocimiento}{52}
\contentsline {subsection}{\numberline {}$<$T2$>$V\IeC {\'\i }as de desarrollo de los embriones de la sociedad del conocimiento}{55}
\contentsline {subsection}{\numberline {}$<$T2$>$\IeC {\textquestiondown }Hacia una sociedad del conocimiento inclusiva y participativa?}{59}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{63}
\contentsline {chapter}{$<$T\IeC {\'\i }tulo de secci\'on$>$Parte II. Comunidades}{67}
\contentsline {chapter}{$<$T1$>$Factores de motivaci\'on y elementos de reconocimiento}{69}
\contentsline {subsection}{\numberline {}$<$T2$>$Factores de motivaci\'on}{70}
\contentsline {paragraph}{$<$T3$>$La programaci\'on como un acto de belleza o de arte}{70}
\contentsline {paragraph}{$<$T3$>$Programar por diversi\'on}{71}
\contentsline {paragraph}{$<$T3$>$Semillero de interacciones sociales}{72}
\contentsline {paragraph}{$<$T3$>$Enfatizando en las ventajas del modelo distribuido, descentralizado de desarrollo}{73}
\contentsline {paragraph}{$<$T3$>$La econom\IeC {\'\i }a del regalo}{76}
\contentsline {paragraph}{$<$T3$>$Impulso \'etico-ideol\'ogico}{77}
\contentsline {paragraph}{$<$T3$>$Incidencia del proyecto en un \'area de inter\'es profesional}{78}
\contentsline {subsection}{\numberline {}$<$T2$>$Elementos de identificaci\'on con la subcultura hacker}{79}
\contentsline {subsection}{\numberline {}$<$T2$>$Paralelos en otros grupos creadores}{81}
\contentsline {paragraph}{$<$T3$>$Paralelos con la comunidad cient\IeC {\'\i }fica}{81}
\contentsline {paragraph}{$<$T3$>$Comunidades de creaci\'on art\IeC {\'\i }stica}{82}
\contentsline {subsection}{\numberline {}$<$T2$>$Diferentes formas y niveles de participaci\'on}{83}
\contentsline {subsection}{\numberline {}$<$T2$>$Jerarquizaci\'on de los individuos en la sociedad}{85}
\contentsline {paragraph}{$<$T3$>$Sistemas de redes sociales}{85}
\contentsline {subsection}{\numberline {}$<$T2$>$Conclusiones}{88}
\contentsline {chapter}{$<$T1$>$La construcci\'on colaborativa del conocimiento desde la \'optica de las comunidades de software libre}{89}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{90}
\contentsline {subsection}{\numberline {}$<$T2$>$Tecnolog\IeC {\'\i }a y comunidades de software libre}{90}
\contentsline {paragraph}{$<$T3$>$Repensar la tecnolog\IeC {\'\i }a}{90}
\contentsline {subsection}{\numberline {}$<$T2$>$El conocimiento es un hecho social}{91}
\contentsline {subsection}{\numberline {}$<$T2$>$Comunidades de software libre}{91}
\contentsline {subsection}{\numberline {}$<$T2$>$Elementos para entender las comunidades de software libre}{92}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{92}
\contentsline {chapter}{$<$T\IeC {\'\i }tulo de secci\'on$>$Parte III. Sociedad}{95}
\contentsline {chapter}{$<$T1$>$Pol\IeC {\'\i }tica 2.0}{97}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{97}
\contentsline {subsection}{\numberline {}$<$T2$>$Una definici\'on de pol\IeC {\'\i }tica 2.0}{99}
\contentsline {subsection}{\numberline {}$<$T2$>$Algunas iniciativas locales}{101}
\contentsline {paragraph}{$<$T3$>$Politika 2.0}{101}
\contentsline {paragraph}{$<$T3$>$Colabora en nuestras ciudades}{101}
\contentsline {paragraph}{$<$T3$>$Ciudadanos2010}{102}
\contentsline {paragraph}{$<$T3$>$Parlio}{102}
\contentsline {paragraph}{$<$T3$>$Ezker Batua Berdeak}{102}
\contentsline {paragraph}{$<$T3$>$Neskateka}{103}
\contentsline {paragraph}{$<$T3$>$arreglaMicalle}{103}
\contentsline {paragraph}{$<$T3$>$Queremos software libre}{103}
\contentsline {paragraph}{$<$T3$>$Sindominio}{103}
\contentsline {paragraph}{$<$T3$>$Indymedia}{103}
\contentsline {paragraph}{$<$T3$>$Hacktivistas}{104}
\contentsline {paragraph}{$<$T3$>$Partido Pirata}{104}
\contentsline {paragraph}{$<$T3$>$Partido de Internet}{105}
\contentsline {paragraph}{$<$T3$>$Irekia}{106}
\contentsline {subsection}{\numberline {}$<$T2$>$Conclusiones}{106}
\contentsline {paragraph}{$<$T3$>$ Caracter\IeC {\'\i }sticas comunes}{106}
\contentsline {paragraph}{$<$T3$>$Tipos de grupos}{107}
\contentsline {chapter}{$<$T1$>$Analfabetizaci\'on inform\'atica o \IeC {\textquestiondown }por qu\'e los programas privativos fomentan la analfabetizaci\'on?}{109}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{109}
\contentsline {subsection}{\numberline {}$<$T2$>$\IeC {\textquestiondown }Qu\'e entendemos por alfabetizaci\'on?}{110}
\contentsline {subsection}{\numberline {}$<$T2$>$Contenido y estructura}{111}
\contentsline {subsection}{\numberline {}$<$T2$>$El futuro en el jard\IeC {\'\i }n de infantes}{112}
\contentsline {subsection}{\numberline {}$<$T2$>$Perspectivas y propuestas}{114}
\contentsline {chapter}{$<$T1$>$Construyendo metodolog\IeC {\'\i }as para la infoinclusi\'on}{117}
\contentsline {subsection}{\numberline {}$<$T2$>$Punto de encuentro: la infoinclusi\'on}{118}
\contentsline {subsection}{\numberline {}$<$T2$>$La historia personal y organizativa}{122}
\contentsline {subsection}{\numberline {}$<$T2$>$El enfoque de g\'enero}{123}
\contentsline {subsection}{\numberline {}$<$T2$>$Experiencias}{125}
\contentsline {paragraph}{$<$T3$>$Usos estrat\'egicos de internet con campesinos }{126}
\contentsline {paragraph}{$<$T3$>$Manejo seguro de la informaci\'on con dos organizaciones que trabajan en derechos humanos}{129}
\contentsline {subsection}{\numberline {}$<$T2$>$Notas finales}{133}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{133}
\contentsline {chapter}{$<$T\IeC {\'\i }tulo de secci\'on$>$Parte IV. Ap\'endices}{137}
\contentsline {chapter}{$<$T1$>$Ap\'endice A. Educaci\'on y software libre}{139}
\contentsline {subsection}{\numberline {}$<$T2$>$ Eje tecnol\'ogico}{141}
\contentsline {paragraph}{$<$T3$>$\IeC {\textquestiondown }Software libre en la educaci\'on o educaci\'on con software libre?}{141}
\contentsline {paragraph}{$<$T3$>$Argumentos del eje tecnol\'ogico, de la ingenuidad educativa}{143}
\contentsline {subsection}{\numberline {}$<$T2$>$Eje \'etico y cognitivo, el software libre como herramienta del pensamiento humano}{145}
\contentsline {paragraph}{$<$T3$>$De vuelta a lo importante, la educaci\'on}{145}
\contentsline {paragraph}{$<$T3$>$Educaci\'on camino a los bienes comunes}{145}
\contentsline {paragraph}{$<$T3$>$De la cultura libre a la educaci\'on libre}{149}
\contentsline {paragraph}{$<$T3$>$Bienes culturales libres: dibujando el futuro}{151}
\contentsline {chapter}{$<$T1$>$Ap\'endice B. Sugar}{153}
\contentsline {subsection}{\numberline {}$<$T2$>$Construccionismo: un enfoque educativo centrado en el aprendiz}{153}
\contentsline {subsection}{\numberline {}$<$T2$>$Los ni\~nos no son oficinistas}{155}
\contentsline {subsection}{\numberline {}$<$T2$>$Actividades para construir y compartir}{157}
\contentsline {subsection}{\numberline {}$<$T2$>$Abramos la educaci\'on}{159}
\contentsline {subsection}{\numberline {}$<$T2$>$Evidencia}{161}
\contentsline {subsection}{\numberline {}$<$T2$>$Sugar y su implementaci\'on en escuelas}{163}
\contentsline {subsection}{\numberline {}$<$T2$>$Cierre}{165}
\contentsline {chapter}{$<$T1$>$Ap\'endice C. Voto electr\'onico: \IeC {\textquestiondown }Qui\'en tiene realmente la decisi\'on?}{167}
\contentsline {subsection}{\numberline {}$<$T2$>$Disminuci\'on de costos}{168}
\contentsline {subsection}{\numberline {}$<$T2$>$Agilidad en la obtenci\'on de resultados}{170}
\contentsline {subsection}{\numberline {}$<$T2$>$Confiabilidad de los actores}{171}
\contentsline {subsection}{\numberline {}$<$T2$>$Votos blancos y nulos: expresi\'on leg\IeC {\'\i }tima del ciudadano}{174}
\contentsline {subsection}{\numberline {}$<$T2$>$Experiencias internacionales}{175}
\contentsline {chapter}{$<$T1$>$Ap\'endice D. Traducci\'on de herramientas para revaloraci\'on y rescate de la lectura tradicional}{179}
\contentsline {subsection}{\numberline {}$<$T2$>$Exposici\'on}{179}
\contentsline {subsection}{\numberline {}$<$T2$>$Sesi\'on de preguntas y respuestas}{185}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{191}
