%% Archivo: FCmathbook.cls
%%

\NeedsTeXFormat{LaTeX2e}
\def\fileversion{v.1.0.4}\def\filedate{2007/01/25}
\ProvidesClass{FCmathbook}[\filedate\space\fileversion\space%
 Dise�o para los libros de matem�ticas de la Facultad de Ciencias UNAM]%%
 
\LoadClass[11pt]{book}
\RequirePackage{amsmath,amssymb,amsfonts,amsthm}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\RequirePackage[ignoremp,papersize={14cm,21cm},textwidth=10.2cm,textheight=19.5cm,margin=1.5cm, %14.5cm - 23cm,margin=2cm, 
       includeall,nomarginpar,centering]{geometry} %textwidth=12.5cm,textheight=18cm (originalmente)
\RequirePackage{graphicx}
\RequirePackage[norule,splitrule]{footmisc}
\RequirePackage[cam,letter,dvips,center]{crop}%[cam,letter,dvips]

%%*** Redise�o del encabezado
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[LO]{\hfill{\scshape \nouppercase{\leftmark}}\hfill{\large\thepage}}
\fancyhead[RE]{{\large\thepage}\hfill{\scshape \nouppercase{\leftmark}}\hfill}
%\fancyhead[RE]{{\large\thepage}\hfill{\scshape \nouppercase{\rightmark}}\hfill} %%%%QUITAR
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\splitfootnoterule}{\kern-3\p@ \rule[3pt]{2.5cm}{1pt}}

%%*** Redise�o del formato del t�tulo del cap�tulo
\titleformat{\chapter}[display]{\normalfont\Large\scshape}{\chaptertitlename\ \thechapter}{0pt}{\huge\bfseries}{}
\titlespacing{\chapter}{0pt}{0pt}{*20}


\endinput
