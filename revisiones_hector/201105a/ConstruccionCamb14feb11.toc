\select@language {spanish}
\contentsline {chapter}{Introducci\'on}{9}
\contentsline {section}{El proyecto}{10}
\contentsline {section}{Forma de trabajo}{10}
\contentsline {subsection}{Esquema de colaboraci\'on}{11}
\contentsline {subsection}{Manejo de referencias}{12}
\contentsline {section}{Implementaci\'on t\'ecnica}{15}
\contentsline {section}{Acerca de los autores}{18}
\contentsline {subsection}{Beatriz Busaniche (Argentina)}{18}
\contentsline {subsection}{H\'ector Colina (Venezuela)}{19}
\contentsline {subsection}{Carolina Flores (Costa Rica)}{19}
\contentsline {subsection}{Antonio Galindo (M\'exico)}{20}
\contentsline {subsection}{Alejandro Miranda (M\'exico)}{20}
\contentsline {subsection}{Sergio Ord\'o\~nez (M\'exico)}{20}
\contentsline {subsection}{Lila Pagola (Argentina)}{21}
\contentsline {subsection}{Marko Txopitea (Euskal Herria)}{21}
\contentsline {subsection}{\'Erika Valverde (Costa Rica)}{22}
\contentsline {subsection}{Gunnar Wolf (M\'exico)}{22}
\contentsline {part}{I\hspace {1em}Software y medios libres}{25}
\contentsline {chapter}{Software libre y construcci\'on democr\'atica de la sociedad}{27}
\contentsline {section}{Introducci\'on}{28}
\contentsline {subsection}{La historia del conocimiento}{28}
\contentsline {subsection}{El c\'omputo como expresi\'on del conocimiento}{32}
\contentsline {subsection}{\IeC {\textquestiondown }Qu\'e es una sociedad democr\'atica?}{34}
\contentsline {subsection}{Del software al conocimiento}{37}
\contentsline {section}{El software libre en tanto movimiento social}{40}
\contentsline {section}{Conclusiones}{45}
\contentsline {chapter}{Esquemas permisivos de licenciamiento en la creaci\'on art\IeC {\'\i }stica}{47}
\contentsline {section}{Introducci\'on}{47}
\contentsline {section}{Posiciones respecto de los derechos de autor y de copia en el arte}{49}
\contentsline {subsection}{Actitud copyleft en el arte}{53}
\contentsline {subsection}{Reacciones desde la industria cultural y las gestoras de derechos de autor}{54}
\contentsline {section}{Cr\IeC {\'\i }tica a la noci\'on de autor: antecedentes en la historia del arte}{54}
\contentsline {section}{Efecto copyleft avant la lettre: c\'omo explicar el copyleft cuando todos lo practicamos}{56}
\contentsline {subsection}{1984, m\'as ac\'a de Orwell: Stallman y Jobs}{57}
\contentsline {subsection}{Los problemas de la naturalizaci\'on: el doble est\'andar}{61}
\contentsline {subsection}{La extra\~neza en la discusi\'on sobre el copyleft}{62}
\contentsline {subsection}{Esquema de colaboraci\'on}{64}
\contentsline {section}{Convergencia de ideas en los modelos alternativos de circulaci\'on que propone el movimiento de software libre desde otras \'areas de la producci\'on cultural}{66}
\contentsline {subsection}{Potenciales de convergencia en una \'epoca de redefiniciones}{67}
\contentsline {paragraph}{$<$T4$>$La afinidad ideol\'ogica de las propuestas: develar el desconocimiento mutuo}{67}
\contentsline {paragraph}{$<$T4$>$Propuestas y proyecciones}{70}
\contentsline {paragraph}{$<$T4$>$Sobre las distancias autor-receptor}{71}
\contentsline {subsection}{Aspectos en debate sobre las licencias permisivas en el arte}{71}
\contentsline {paragraph}{$<$T4$>$Diversidad y calidad de las obras de libre circulaci\'on}{71}
\contentsline {paragraph}{$<$T4$>$Puntos problem\'aticos para los autores en las licencias permisivas}{72}
\contentsline {paragraph}{$<$T4$>$Desconocimiento del copyright y, por extensi\'on, errores de interpretaci\'on sobre el copyleft}{73}
\contentsline {paragraph}{$<$T4$>$Desinter\'es por la mara\~na t\'ecnico-legal}{74}
\contentsline {paragraph}{$<$T4$>$Usos comerciales}{74}
\contentsline {paragraph}{$<$T4$>$Obras derivadas}{76}
\contentsline {paragraph}{$<$T4$>$La obra derivada sin control: o la preocupaci\'on por evitar ``ciertas'' derivaciones}{77}
\contentsline {chapter}{Fundamentos te\'oricos y ubicaci\'on hist\'orica de la econom\IeC {\'\i }a y sociedad del conocimiento}{79}
\contentsline {section}{Introducci\'on}{79}
\contentsline {section}{Econom\IeC {\'\i }a y sociedad del conocimiento}{81}
\contentsline {section}{V\IeC {\'\i }as de desarrollo de los embriones de la sociedad del conocimiento}{86}
\contentsline {section}{\IeC {\textquestiondown }Hacia una sociedad del conocimiento inclusiva y participativa?}{92}
\contentsline {section}{Referencias}{100}
\contentsline {part}{II\hspace {1em}Comunidades}{107}
\contentsline {chapter}{Factores de motivaci\'on y elementos de reconocimiento}{109}
\contentsline {section}{Factores de motivaci\'on}{110}
\contentsline {subsection}{La programaci\'on como un acto de belleza o de arte}{111}
\contentsline {subsection}{Programar por diversi\'on}{113}
\contentsline {subsection}{Semillero de interacciones sociales}{114}
\contentsline {subsection}{Enfatizando en las ventajas del modelo distribuido, descentralizado de desarrollo}{116}
\contentsline {subsection}{La econom\IeC {\'\i }a del regalo}{121}
\contentsline {subsection}{Impulso \'etico-ideol\'ogico}{122}
\contentsline {subsection}{Incidencia del proyecto en un \'area de inter\'es profesional}{124}
\contentsline {section}{Elementos de identificaci\'on con la subcultura hacker}{125}
\contentsline {section}{Paralelos en otros grupos creadores}{128}
\contentsline {subsection}{Paralelos con la comunidad cient\IeC {\'\i }fica}{129}
\contentsline {subsection}{Comunidades de creaci\'on art\IeC {\'\i }stica}{130}
\contentsline {section}{Diferentes formas y niveles de participaci\'on}{132}
\contentsline {section}{Jerarquizaci\'on de los individuos en la sociedad}{136}
\contentsline {subsection}{Sistemas de redes sociales}{136}
\contentsline {section}{Conclusiones}{141}
\contentsline {chapter}{La construcci\'on colaborativa del conocimiento desde la \'optica de las comunidades de {\it software} libre}{143}
\contentsline {section}{Introducci\'on}{144}
\contentsline {section}{Tecnolog\IeC {\'\i }a y comunidades de software libre}{145}
\contentsline {subsection}{Repensar la tecnolog\IeC {\'\i }a}{145}
\contentsline {section}{El conocimiento es un hecho social}{147}
\contentsline {section}{Comunidades de software libre}{147}
\contentsline {section}{Elementos para entender las comunidades de software libre}{148}
\contentsline {section}{Referencias}{149}
\contentsline {part}{III\hspace {1em}Sociedad}{153}
\contentsline {chapter}{Pol\IeC {\'\i }tica 2.0}{155}
\contentsline {section}{Introducci\'on}{155}
\contentsline {section}{Una definici\'on de pol\IeC {\'\i }tica 2.0}{159}
\contentsline {section}{Algunas iniciativas locales}{162}
\contentsline {subsection}{Politika 2.0}{162}
\contentsline {subsection}{Colabora en nuestras ciudades}{162}
\contentsline {subsection}{Ciudadanos 2010}{163}
\contentsline {subsection}{Parlio}{163}
\contentsline {subsection}{Ezker Batua Berdeak}{164}
\contentsline {subsection}{Neskateka}{164}
\contentsline {subsection}{arreglaMicalle}{165}
\contentsline {subsection}{Queremos software libre}{165}
\contentsline {subsection}{Sindominio}{166}
\contentsline {subsection}{Indymedia}{166}
\contentsline {subsection}{Hacktivistas}{167}
\contentsline {subsection}{Partido Pirata}{167}
\contentsline {subsection}{Partido de Internet}{168}
\contentsline {subsection}{Irekia}{170}
\contentsline {section}{Conclusiones}{170}
\contentsline {subsection}{Caracter\IeC {\'\i }sticas comunes}{170}
\contentsline {subsection}{Tipos de grupos}{171}
\contentsline {chapter}{Analfabetizaci\'on inform\'atica o \IeC {\textquestiondown }por qu\'e los programas privativos fomentan la analfabetizaci\'on?}{175}
\contentsline {section}{Introducci\'on}{175}
\contentsline {section}{\IeC {\textquestiondown }Qu\'e entendemos por alfabetizaci\'on?}{177}
\contentsline {section}{Contenido y estructura}{179}
\contentsline {section}{El futuro en el jard\IeC {\'\i }n de infantes}{180}
\contentsline {section}{Perspectivas y propuestas}{183}
\contentsline {chapter}{Construyendo metodolog\IeC {\'\i }as para la infoinclusi\'on}{187}
\contentsline {section}{Punto de encuentro: la infoinclusi\'on}{188}
\contentsline {section}{La historia personal y organizativa}{195}
\contentsline {section}{El enfoque de g\'enero}{198}
\contentsline {section}{Experiencias}{201}
\contentsline {subsection}{Usos estrat\'egicos de internet con campesinos}{201}
\contentsline {subsection}{Manejo seguro de la informaci\'on con dos organizaciones que trabajan en derechos humanos}{207}
\contentsline {section}{Notas finales}{213}
\contentsline {section}{Referencias}{214}
\contentsline {part}{IV\hspace {1em}Ap\'endices}{219}
\contentsline {chapter}{Ap\'endice A. Educaci\'on y {\it software} libre}{221}
\contentsline {section}{Eje tecnol\'ogico}{224}
\contentsline {subsection}{\IeC {\textquestiondown }Software libre en la educaci\'on o educaci\'on con software libre?}{224}
\contentsline {subsection}{Argumentos del eje tecnol\'ogico, de la ingenuidad educativa}{228}
\contentsline {section}{Eje \'etico y cognitivo, el software libre como herramienta del pensamiento humano}{230}
\contentsline {subsection}{De vuelta a lo importante, la educaci\'on}{230}
\contentsline {subsection}{Educaci\'on camino a los bienes comunes}{231}
\contentsline {subsection}{De la cultura libre a la educaci\'on libre}{237}
\contentsline {subsection}{Bienes culturales libres: dibujando el futuro}{241}
\contentsline {chapter}{Ap\'endice B. Sugar}{243}
\contentsline {section}{Construccionismo: un enfoque educativo centrado en el aprendiz}{244}
\contentsline {section}{Los ni\~nos no son oficinistas}{247}
\contentsline {section}{Actividades para construir y compartir}{250}
\contentsline {section}{Abramos la educaci\'on}{254}
\contentsline {section}{Evidencia}{258}
\contentsline {section}{Sugar y su implementaci\'on en escuelas}{263}
\contentsline {section}{Cierre}{265}
\contentsline {chapter}{Ap\'endice C. Voto electr\'onico: \IeC {\textquestiondown }Qui\'en tiene realmente la decisi\'on?}{269}
\contentsline {section}{Disminuci\'on de costos}{271}
\contentsline {section}{Agilidad en la obtenci\'on de resultados}{274}
\contentsline {section}{Confiabilidad de los actores}{276}
\contentsline {section}{Votos blancos y nulos: expresi\'on leg\IeC {\'\i }tima del ciudadano}{280}
\contentsline {section}{Experiencias internacionales}{282}
\contentsline {chapter}{Ap\'endice D. Traducci\'on de herramientas para revaloraci\'on y rescate de la lectura tradicional}{287}
\contentsline {section}{Exposici\'on}{287}
\contentsline {section}{Sesi\'on de preguntas y respuestas}{297}
\contentsline {section}{Referencias}{307}
