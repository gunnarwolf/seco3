%% Archivo: FCmathbook.cls
%%
%% Estilo para los libros de matem�ticas publicados por la
%% Facultad de Ciencias.
%%
%% Elaborado por: Ricardo Rios Perez y H�ctor Miguel Cejudo Camacho
%%
%% Marzo 2007

%% POR CORREGIR
%%
%% - Los ejercicios solo se pueden referenciar por n�mero de ejercicio, el autor
%% tiene que referenciar la secci�n y cap�tulo manualmente.
%% - Espacios anteriores y posteriores a los ejercicios. (podr�a mejorar)
%%
%% - CAPTION de figuras a 10pt
%%
%% - Ambientes matem�ticos en negritas, solo demostraci�n en versales.

\NeedsTeXFormat{LaTeX2e}
\def\fileversion{v.1.0.4}\def\filedate{2007/01/25}
\ProvidesClass{FCmathbook}[\filedate\space\fileversion\space%
 Dise�o para los libros de matem�ticas de la Facultad de Ciencias UNAM]%%
 
\LoadClass[11pt]{book}
\RequirePackage{amsmath,amssymb,amsfonts,amsthm}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\RequirePackage[ignoremp,papersize={16cm,23cm},textwidth=12cm,textheight=18.5cm,margin=1.7cm, %14.5cm - 23cm,margin=2cm, 
       includeall,nomarginpar,centering]{geometry} %textwidth=12.5cm,textheight=18cm (originalmente)
\RequirePackage{graphicx}
\RequirePackage[norule,splitrule]{footmisc}
\RequirePackage[cam,letter,dvips,center]{crop}%[cam,letter,dvips]

%%*** Redise�o del encabezado
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[LO]{\hfill{\scshape \nouppercase{\leftmark}}\hfill{\large\thepage}}
\fancyhead[RE]{{\large\thepage}\hfill{\scshape \nouppercase{\leftmark}}\hfill}
%\fancyhead[RE]{{\large\thepage}\hfill{\scshape \nouppercase{\rightmark}}\hfill} %%%%QUITAR
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\splitfootnoterule}{\kern-3\p@ \rule[3pt]{2.5cm}{1pt}}

%%*** Redise�o del formato del t�tulo del cap�tulo
\titleformat{\chapter}[display]{\normalfont\Large\scshape}{\chaptertitlename\ \thechapter}{0pt}{\huge\bfseries}{}
\titlespacing{\chapter}{0pt}{0pt}{*20}

%%*** Ambientes

\theoremstyle{plain}
%%** Teorema
\newtheorem{teorema}{Teorema}[chapter]
%%** Proposici�n
\newtheorem{proposicion}[teorema]{Proposici\'on}
%%** Lema
\newtheorem{lema}[teorema]{Lema}

%\newtheorem{ejemplo}[teorema]{Ejemplo}
\newcounter{ejemplo}[section]   % Asociado a la secci�n.  Nueva secci�n->Nuevos ejemplos
\newenvironment{ejemplo}
{\refstepcounter{ejemplo}\textbf{Ejemplo \thesection.\theejemplo.}\it}


%%** Observaci�n
\newenvironment{observacion}{\medskip\noindent\textbf{Observaci\'on}}{\medskip}
%%** Demostraci�n
\newenvironment{demostracion}{\medskip\noindent\textsc{Demostraci\'on:}}{\medskip}
   
\theoremstyle{definition}
%%** Definici�n
\newtheorem{definicion}[teorema]{Definici\'on}

%%** Ejercicios
\newcounter{contejerc}[section]   % Asociado a la secci�n.  Nueva secci�n->Nuevos ejercicios
\newenvironment{ejercicio}
{\refstepcounter{contejerc}\smallskip\noindent\small\textsc{Ejercicio \thesection.\thecontejerc.}\normalfont}
{\normalsize\smallskip}

%%*** Lista de Ejercicios

%%*** Referencias a ejercicios
%%% ��� NO FUNCIONA !!! Como uno quisiera 8)
\newcommand{\refejc}[1]{\thesection.\ref{#1}}

%%*** Redise�o de la portada

\endinput
