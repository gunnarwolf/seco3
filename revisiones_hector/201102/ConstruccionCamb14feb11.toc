\select@language {spanish}
\contentsline {chapter}{Introducci\'on}{7}
\contentsline {section}{\numberline {}El proyecto}{8}
\contentsline {section}{\numberline {}Forma de trabajo}{8}
\contentsline {subsection}{\numberline {}Esquema de colaboraci\'on}{8}
\contentsline {subsection}{\numberline {}Manejo de referencias}{10}
\contentsline {section}{\numberline {}Implementaci\'on t\'ecnica}{11}
\contentsline {section}{\numberline {}Acerca de los autores}{14}
\contentsline {subsection}{\numberline {}Beatriz Busaniche (Argentina)}{14}
\contentsline {subsection}{\numberline {}H\'ector Colina (Venezuela)}{15}
\contentsline {subsection}{\numberline {}Carolina Flores (Costa Rica)}{15}
\contentsline {subsection}{\numberline {}Antonio Galindo (M\'exico)}{16}
\contentsline {subsection}{\numberline {}Alejandro Miranda (M\'exico)}{16}
\contentsline {subsection}{\numberline {}Sergio Ord\'o\~nez (M\'exico)}{16}
\contentsline {subsection}{\numberline {}Lila Pagola (Argentina)}{17}
\contentsline {subsection}{\numberline {}Marko Txopitea (Euskal Herria)}{17}
\contentsline {subsection}{\numberline {}\'Erika Valverde (Costa Rica)}{17}
\contentsline {subsection}{\numberline {}Gunnar Wolf (M\'exico)}{18}
\contentsline {part}{I\hspace {1em}Parte I. Software y medios libres}{19}
\contentsline {chapter}{Software libre y construcci\'on democr\'atica de la sociedad}{21}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{21}
\contentsline {paragraph}{$<$T3$>$La historia del conocimiento}{22}
\contentsline {paragraph}{$<$T3$>$El c\'omputo como expresi\'on del conocimiento}{24}
\contentsline {paragraph}{$<$T3$>$\IeC {\textquestiondown }Qu\'e es una sociedad democr\'atica?}{26}
\contentsline {paragraph}{$<$T3$>$Del software al conocimiento}{29}
\contentsline {subsection}{\numberline {}$<$T2$>$El software libre en tanto movimiento social}{31}
\contentsline {subsection}{\numberline {}$<$T2$>$Conclusiones}{35}
\contentsline {chapter}{$<$T1$>$Esquemas permisivos de licenciamiento en la creaci\'on art\IeC {\'\i }stica}{37}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{37}
\contentsline {subsection}{\numberline {}$<$T2$>$Posiciones respecto de los derechos de autor y de copia en el arte}{39}
\contentsline {paragraph}{$<$T3$>$Actitud copyleft en el arte}{41}
\contentsline {paragraph}{$<$T3$>$Reacciones desde la industria cultural y las gestoras de derechos de autor}{42}
\contentsline {subsection}{\numberline {}$<$T2$>$Cr\IeC {\'\i }tica a la noci\'on de autor: antecedentes en la historia del arte}{42}
\contentsline {subsection}{\numberline {}$<$T2$>$Efecto copyleft avant la lettre: c\'omo explicar el copyleft cuando todos lo practicamos}{43}
\contentsline {paragraph}{$<$T3$>$1984, m\'as ac\'a de Orwell: Stallman y Jobs}{44}
\contentsline {paragraph}{$<$T3$>$Los problemas de la naturalizaci\'on: el doble est\'andar}{47}
\contentsline {paragraph}{$<$T3$>$La extra\~neza en la discusi\'on sobre el copyleft}{48}
\contentsline {paragraph}{$<$T3$>$Efecto performativo del copyleft}{49}
\contentsline {subsection}{\numberline {}$<$T2$>$Convergencia de ideas en los modelos alternativos de circulaci\'on que propone el movimiento de software libre desde otras \'areas de la producci\'on cultural}{51}
\contentsline {paragraph}{$<$T3$>$Potenciales de convergencia en una \'epoca de redefiniciones}{51}
\contentsline {paragraph}{$<$T4$>$La afinidad ideol\'ogica de las propuestas: develar el desconocimiento mutuo}{52}
\contentsline {paragraph}{$<$T4$>$Propuestas y proyecciones}{54}
\contentsline {paragraph}{$<$T4$>$Sobre las distancias autor-receptor}{54}
\contentsline {paragraph}{$<$T3$>$Aspectos en debate sobre las licencias permisivas en el arte}{55}
\contentsline {paragraph}{$<$T4$>$Diversidad y calidad de las obras de libre circulaci\'on}{55}
\contentsline {paragraph}{$<$T4$>$Puntos problem\'aticos para los autores en las licencias permisivas}{55}
\contentsline {paragraph}{$<$T4$>$Desconocimiento del copyright y, por extensi\'on, errores de interpretaci\'on sobre el copyleft}{56}
\contentsline {paragraph}{$<$T4$>$Desinter\'es por la mara\~na t\'ecnico-legal}{56}
\contentsline {paragraph}{$<$T4$>$Usos comerciales}{57}
\contentsline {paragraph}{$<$T4$>$Obras derivadas}{58}
\contentsline {paragraph}{$<$T4$>$La obra derivada sin control: o la preocupaci\'on por evitar ``ciertas'' derivaciones}{59}
\contentsline {chapter}{$<$T1$>$Fundamentos te\'oricos y ubicaci\'on hist\'orica de la econom\IeC {\'\i }a y sociedad del conocimiento}{61}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{61}
\contentsline {subsection}{\numberline {}$<$T2$>$Econom\IeC {\'\i }a y sociedad del conocimiento}{62}
\contentsline {subsection}{\numberline {}$<$T2$>$V\IeC {\'\i }as de desarrollo de los embriones de la sociedad del conocimiento}{67}
\contentsline {subsection}{\numberline {}$<$T2$>$\IeC {\textquestiondown }Hacia una sociedad del conocimiento inclusiva y participativa?}{71}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{78}
\contentsline {chapter}{$<$T\IeC {\'\i }tulo de secci\'on$>$Parte II. Comunidades}{83}
\contentsline {chapter}{$<$T1$>$Factores de motivaci\'on y elementos de reconocimiento}{85}
\contentsline {subsection}{\numberline {}$<$T2$>$Factores de motivaci\'on}{86}
\contentsline {paragraph}{$<$T3$>$La programaci\'on como un acto de belleza o de arte}{86}
\contentsline {paragraph}{$<$T3$>$Programar por diversi\'on}{88}
\contentsline {paragraph}{$<$T3$>$Semillero de interacciones sociales}{89}
\contentsline {paragraph}{$<$T3$>$Enfatizando en las ventajas del modelo distribuido, descentralizado de desarrollo}{90}
\contentsline {paragraph}{$<$T3$>$La econom\IeC {\'\i }a del regalo}{94}
\contentsline {paragraph}{$<$T3$>$Impulso \'etico-ideol\'ogico}{95}
\contentsline {paragraph}{$<$T3$>$Incidencia del proyecto en un \'area de inter\'es profesional}{97}
\contentsline {subsection}{\numberline {}$<$T2$>$Elementos de identificaci\'on con la subcultura hacker}{97}
\contentsline {subsection}{\numberline {}$<$T2$>$Paralelos en otros grupos creadores}{100}
\contentsline {paragraph}{$<$T3$>$Paralelos con la comunidad cient\IeC {\'\i }fica}{100}
\contentsline {paragraph}{$<$T3$>$Comunidades de creaci\'on art\IeC {\'\i }stica}{101}
\contentsline {subsection}{\numberline {}$<$T2$>$Diferentes formas y niveles de participaci\'on}{103}
\contentsline {subsection}{\numberline {}$<$T2$>$Jerarquizaci\'on de los individuos en la sociedad}{106}
\contentsline {paragraph}{$<$T3$>$Sistemas de redes sociales}{106}
\contentsline {subsection}{\numberline {}$<$T2$>$Conclusiones}{110}
\contentsline {chapter}{$<$T1$>$La construcci\'on colaborativa del conocimiento desde la \'optica de las comunidades de software libre}{111}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{112}
\contentsline {subsection}{\numberline {}$<$T2$>$Tecnolog\IeC {\'\i }a y comunidades de software libre}{112}
\contentsline {paragraph}{$<$T3$>$Repensar la tecnolog\IeC {\'\i }a}{113}
\contentsline {subsection}{\numberline {}$<$T2$>$El conocimiento es un hecho social}{114}
\contentsline {subsection}{\numberline {}$<$T2$>$Comunidades de software libre}{114}
\contentsline {subsection}{\numberline {}$<$T2$>$Elementos para entender las comunidades de software libre}{115}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{115}
\contentsline {chapter}{$<$T\IeC {\'\i }tulo de secci\'on$>$Parte III. Sociedad}{119}
\contentsline {chapter}{$<$T1$>$Pol\IeC {\'\i }tica 2.0}{121}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{121}
\contentsline {subsection}{\numberline {}$<$T2$>$Una definici\'on de pol\IeC {\'\i }tica 2.0}{124}
\contentsline {subsection}{\numberline {}$<$T2$>$Algunas iniciativas locales}{126}
\contentsline {paragraph}{$<$T3$>$Politika 2.0}{127}
\contentsline {paragraph}{$<$T3$>$Colabora en nuestras ciudades}{127}
\contentsline {paragraph}{$<$T3$>$Ciudadanos2010}{127}
\contentsline {paragraph}{$<$T3$>$Parlio}{127}
\contentsline {paragraph}{$<$T3$>$Ezker Batua Berdeak}{128}
\contentsline {paragraph}{$<$T3$>$Neskateka}{128}
\contentsline {paragraph}{$<$T3$>$arreglaMicalle}{129}
\contentsline {paragraph}{$<$T3$>$Queremos software libre}{129}
\contentsline {paragraph}{$<$T3$>$Sindominio}{129}
\contentsline {paragraph}{$<$T3$>$Indymedia}{129}
\contentsline {paragraph}{$<$T3$>$Hacktivistas}{130}
\contentsline {paragraph}{$<$T3$>$Partido Pirata}{131}
\contentsline {paragraph}{$<$T3$>$Partido de Internet}{131}
\contentsline {paragraph}{$<$T3$>$Irekia}{132}
\contentsline {subsection}{\numberline {}$<$T2$>$Conclusiones}{132}
\contentsline {paragraph}{$<$T3$>$ Caracter\IeC {\'\i }sticas comunes}{133}
\contentsline {paragraph}{$<$T3$>$Tipos de grupos}{134}
\contentsline {chapter}{$<$T1$>$Analfabetizaci\'on inform\'atica o \IeC {\textquestiondown }por qu\'e los programas privativos fomentan la analfabetizaci\'on?}{137}
\contentsline {subsection}{\numberline {}$<$T2$>$Introducci\'on}{137}
\contentsline {subsection}{\numberline {}$<$T2$>$\IeC {\textquestiondown }Qu\'e entendemos por alfabetizaci\'on?}{139}
\contentsline {subsection}{\numberline {}$<$T2$>$Contenido y estructura}{140}
\contentsline {subsection}{\numberline {}$<$T2$>$El futuro en el jard\IeC {\'\i }n de infantes}{141}
\contentsline {subsection}{\numberline {}$<$T2$>$Perspectivas y propuestas}{143}
\contentsline {chapter}{$<$T1$>$Construyendo metodolog\IeC {\'\i }as para la infoinclusi\'on}{147}
\contentsline {subsection}{\numberline {}$<$T2$>$Punto de encuentro: la infoinclusi\'on}{148}
\contentsline {subsection}{\numberline {}$<$T2$>$La historia personal y organizativa}{153}
\contentsline {subsection}{\numberline {}$<$T2$>$El enfoque de g\'enero}{156}
\contentsline {subsection}{\numberline {}$<$T2$>$Experiencias}{158}
\contentsline {paragraph}{$<$T3$>$Usos estrat\'egicos de internet con campesinos }{158}
\contentsline {paragraph}{$<$T3$>$Manejo seguro de la informaci\'on con dos organizaciones que trabajan en derechos humanos}{163}
\contentsline {subsection}{\numberline {}$<$T2$>$Notas finales}{168}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{169}
\contentsline {chapter}{$<$T\IeC {\'\i }tulo de secci\'on$>$Parte IV. Ap\'endices}{173}
\contentsline {chapter}{$<$T1$>$Ap\'endice A. Educaci\'on y software libre}{175}
\contentsline {subsection}{\numberline {}$<$T2$>$ Eje tecnol\'ogico}{177}
\contentsline {paragraph}{$<$T3$>$\IeC {\textquestiondown }Software libre en la educaci\'on o educaci\'on con software libre?}{177}
\contentsline {paragraph}{$<$T3$>$Argumentos del eje tecnol\'ogico, de la ingenuidad educativa}{180}
\contentsline {subsection}{\numberline {}$<$T2$>$Eje \'etico y cognitivo, el software libre como herramienta del pensamiento humano}{182}
\contentsline {paragraph}{$<$T3$>$De vuelta a lo importante, la educaci\'on}{182}
\contentsline {paragraph}{$<$T3$>$Educaci\'on camino a los bienes comunes}{183}
\contentsline {paragraph}{$<$T3$>$De la cultura libre a la educaci\'on libre}{188}
\contentsline {paragraph}{$<$T3$>$Bienes culturales libres: dibujando el futuro}{191}
\contentsline {chapter}{$<$T1$>$Ap\'endice B. Sugar}{193}
\contentsline {subsection}{\numberline {}$<$T2$>$Construccionismo: un enfoque educativo centrado en el aprendiz}{193}
\contentsline {subsection}{\numberline {}$<$T2$>$Los ni\~nos no son oficinistas}{196}
\contentsline {subsection}{\numberline {}$<$T2$>$Actividades para construir y compartir}{198}
\contentsline {subsection}{\numberline {}$<$T2$>$Abramos la educaci\'on}{201}
\contentsline {subsection}{\numberline {}$<$T2$>$Evidencia}{204}
\contentsline {subsection}{\numberline {}$<$T2$>$Sugar y su implementaci\'on en escuelas}{207}
\contentsline {subsection}{\numberline {}$<$T2$>$Cierre}{209}
\contentsline {chapter}{$<$T1$>$Ap\'endice C. Voto electr\'onico: \IeC {\textquestiondown }Qui\'en tiene realmente la decisi\'on?}{211}
\contentsline {subsection}{\numberline {}$<$T2$>$Disminuci\'on de costos}{213}
\contentsline {subsection}{\numberline {}$<$T2$>$Agilidad en la obtenci\'on de resultados}{215}
\contentsline {subsection}{\numberline {}$<$T2$>$Confiabilidad de los actores}{217}
\contentsline {subsection}{\numberline {}$<$T2$>$Votos blancos y nulos: expresi\'on leg\IeC {\'\i }tima del ciudadano}{220}
\contentsline {subsection}{\numberline {}$<$T2$>$Experiencias internacionales}{221}
\contentsline {chapter}{$<$T1$>$Ap\'endice D. Traducci\'on de herramientas para revaloraci\'on y rescate de la lectura tradicional}{225}
\contentsline {subsection}{\numberline {}$<$T2$>$Exposici\'on}{225}
\contentsline {subsection}{\numberline {}$<$T2$>$Sesi\'on de preguntas y respuestas}{233}
\contentsline {subsection}{\numberline {}$<$T2$>$Referencias}{241}
