\par Las propiedades que poseen las nuevas tecnologías están provocando cambios en diversos ámbitos de las sociedades que hacen uso de ellas. El mantra del software libre, los estándares abiertos, la accesibilidad, la digitalización de la información, ``copyleft'', ``coompetición'', ``prosumidores'', redes sociales digitales, la neutralidad de la red, etc. son conceptos e ideas que resuenan cada vez con mayor fuerza y especialmente el paradigma de la ``web 2.0'' está ayudando a la definición de un nuevo horizonte en el equilibrio entre la democracia representativa y la democracia directa.

\par El objetivo de este trabajo es tratar de definir este nuevo terreno que se ha dado en llamar política 2.0, analizar algunas experiencias que el autor conoce y que ya están en marcha y con las conclusiones extraidas de este trabajo, tratar de ayudar al lector a vislumbrar los caminos que convendría seguir explorando para lograr un democracia de mayor calidad (en ámbitos y/o lugares concretos o a niveles más generales de la sociedad) utilizando en mayor o menor medida las nuevas tecnologías.





\section{Introducción}

\par 

Las nuevas tecnologías de la información y la comunicación \Parencite{793}(NTIC) en general e Internet en especial, poseen una serie de características que están provocando cambios en muy diversos ámbitos de la sociedad (de aquellas sociedades que tienen un acceso amplio a las NTIC). Una de las características que más efectos está teniendo a nivel social, es la posibilidad que tienen los usuarios no sólo de recibir información, sino de enviarla. Es decir, no limitarse a ser meros espectadores, meros consumidores de información, sino a tener la posibilidad de ser productores de información (\emph{prosumer} \Parencite{794}) y compartirla libremente.



\par 

Esto unido a otras características comunes a las nuevas tecnologías como la digitalización de la información (lo que implica almacenamiento y retrasmisión de la información sin pérdida de calidad), la rapidez y la potencia de las comunicaciones, la cada vez mayor facilidad de uso, etcétera, está provocando cambios profundos en diversos sectores. Desde los medios de comunicación, hasta la enseñanza, pasando por el entretenimiento, el comercio, la ingeniería, la ciencia, el arte, la sanidad, la cultura y un largo etcétera. Los cambios son tantos y afectan a tantos niveles, que es frecuente hablar de la sociedad como \emph{sociedad de la información} \Parencite{795} (en contraposición a la sociedad industrial).



\par 

Uno de los primeros sectores en hacer uso de las redes de ordenadores y las nuevas oportunidades de creación, gestión y socialización de la información, fueron los programadores informáticos. Gracias a Internet, fue posible el movimiento del software libre \Parencite{796}. Tras él, otros muchos sectores han comenzado a compartir contenidos, conocimientos y experiencias de forma cada vez más generalizada: los científicos, los alumnos, los profesores, los aficionados a la fotografía, a la música, al vídeo, etc. Actualmente, en el terreno de la propiedad intelectual existe una auténtica revolución, puesto que gracias a la tecnología, los contenidos se han liberado de sus soportes y ya no tiene sentido seguir aplicando las normas de propiedad propias de los objetos físicos.



\par 

Algunos sectores como el del software, han sabido adaptarse ya a los nuevos tiempos, mientras que otros como el de las enciclopedias, se han visto totalmente superadas por los acontecimientos \Parencite{232}. Mientras tanto, otros sectores como el de los medios de comunicación o la industria del entretenimiento, se aferran a sus cada vez más obsoletos modelos de negocio y están ejerciendo una gran presión para tratar de criminalizar la tendencia natural de los seres humanos a compartir. No quieren darse cuenta de que cada año miles de grupos de música deciden distribuir su música libremente por Internet y de que millones de usuarios comparten las fotografías, las noticias y los vídeos que crean.



\par 

En el ámbito del periodismo, ahora que es tan fácil crear blogs, utilizar agregadores de noticias, sindicar contenidos, publicar fotografías, crear una radio por Internet mediante streaming de audio, crear podcasts, grabar, editar y publicar vídeos, etcétera, la prensa tradicional también se está viendo obligada a renovarse para no quedarse relegada.



\par 

El problema de fondo es que gracias a Internet, cada vez más músicos, escritores y creadores de todo tipo, se comunican directamente con la gente a la que le interesa lo que hacen. Cada vez es más fácil completar el ciclo prescindiendo de intermediarios que antes controlaban todo el proceso de creación (discográficas, editores, managers, etc.) Por lo que muchos intermediarios tradicionales deberán o están ya en proceso de reformular su papel en el nuevo escenario.



\par 

Lo mismo podríamos decir de la clase política. Muchos agentes sociales como asociaciones, grupos altermundistas \Parencite{229}, ecologistas, internacionalistas, hacktivistas\footnote{Para más información sobre los diversos \emph{hacklabs} existentes en el mundo, así como el movimiento social que constituyen, consultar \url{http://www.hacklabs.org/es}} y otros movimientos de base, llevan tiempo utilizando Internet como sitio natural donde trabajar en red y potenciar su capacidad de transformación social. Cuanto más se extiende el uso de Internet y de las nuevas tecnologías, más se traslada el debate social y la acción política a ámbitos donde este tipo de debates no suele ser el habitual. Incluso fuera de los gobiernos y los partidos políticos al uso, puesto que los sistemas basados principalmente en la democracia representativa, también añaden intermediarios que a los ojos de los nativos digitales resultan anacrónicos y poco atractivos \Parencite{233}.



\par 

Por si esto fuera poco, el auge de la llamada \emph{web 2.0} \Parencite{797} (blogs, wikis, plataformas de microbloging, etc.), ha propiciado que muchos colectivos y movimientos sociales hayan pasado de utilizar rudimentarias listas de correo, a herramientas que facilitan enormemente el trabajo comunitario y la difusión de diversas propuestas en la red. Incluso comunidades generalistas (no articuladas en torno a ningún movimiento social), al hacer uso de Internet están fuera del control de los lobbies de poder (cosa que no ocurre ni con los medios de comunicación \Parencite{235}, ni con los gobiernos \Parencite{234}) y surgen una y otra vez de forma espontanea muchos temas e iniciativas de caracter social: libertad de expresión, manipulación informativa, censura, privacidad, control social, propiedad intelectual, ecología, etc. Durante los últimos años se está extendiendo el uso de herramientas diseñadas \emph{ex profeso} para la gestión de redes sociales, tanto por parte de empresas (Facebook, Twitter, MySpace, Youtube y similares \Parencite{817}) como por parte de grupos ciudadanos (Identi.ca, Grabgrass, Diaspora, Lorea.cc y similares \Parencite{818}). Aunque las primeras están más extendidas que las segundas, ambas están siendo utilizadas cada vez más con objetivos políticos y sociales (organización de manifestaciones y otros eventos, recogidas de firmas, coordinación de protestas antes empresas y gobiernos, etc.). Por todo ello, cabe esperar que en los próximos años, las iniciativas enmarcables dentro de lo que denominamos política 2.0 vaya cobrando cada vez más fuerza.









\section{Una definición de Política 2.0}

\par 

El término Política 2.0 surge de la fusión de dos términos: \textbf{Política + Web 2.0}.



\par 

Es importante aclarar que la Política no es algo exclusivo de los partidos políticos y de los políticos profesionales, porque estos podrían simplemente no existir, sino que es una actividad que incumbe a todos los ciudadanos. Política es un concepto general que no está limitado a ninguna forma de gobierno  \Parencite{798} concreta (como por ejemplo autoritarismo, totalitarismo o democracia). Aunque es usual asociar esta palabra a la forma de gobierno más extendida en la actualidad en los diferentes estados del mundo: la democracia representativa pluripartidista. Pero no hay que olvidar que además de la democracia representativa (existen representantes de la voluntad de los ciudadanos), también existe la democracia directa (el ciudadano expresa su voluntad sin intermediarios). Y además del sistema pluripartidista (varios partidos), también existe el sistema unipartidista (un partido) y el sistema apartidista (no hay ningún partido). Por tanto, el término Política abarca las formas de gobiernos mencionadas y en general todas las formas de gobierno (república, monarquía, sistema presidencial, sistema parlamentario, etc.)



\par 

La Política no es algo exclusivo de los políticos profesionales, porque existan estos o no, la gestión de los asuntos públicos es responsabilidad de todos los cuidadanos y en las sociedades humanas esta gestión se lleva a cabo mediante complejas redes en las que directa o indirectamente participan todos los miembros de dicha sociedad  \Parencite{334}. No en vano, la Real Academia Española al definir la palabra Política no menciona a otro más que al ciudadano: \emph{``Actividad del ciudadano cuando interviene en los asuntos públicos con su opinión, con su voto, o de cualquier otro modo.''}\footnote{Definición del término ``política'' según la Real Academia Española: \url{http://buscon.rae.es/draeI/SrvltObtenerHtml?origen=RAE\&SUPIND=0\&CAREXT=10000\&LEMA=político}).}



\par 

El término \emph{Web 2.0}  \Parencite{797} es un concepto de reciente creación (2004) y se refiere al conocido sistema de páginas interconectadas mediante enlaces de hipertexto (``la web''), pero con el nivel de maduración que se ha logrado en los últimos años y que fomenta la colaboración y el intercambio ágil de información entre los usuarios de una comunidad o red social mediante servicios como \emph{blogs}, \emph{wikis}, plataformas de \emph{microbloging}, etc.



\par 

Teniendo en cuenta todo lo expuesto, la Política 2.0 se puede definir de la siguiente forma:



\begin{quotation}

	\par 

	Actividad del ciudadano cuando interviene en los asuntos públicos con su opinión, con su voto o de cualquier otro modo, haciendo uso de las capacidades que ofrecen las nuevas tecnologías en general y la web 2.0 en particular.

	

\end{quotation}

\subsection{Otros términos similares}

\par 

Además del término Política 2.0, han surgido otros términos también nuevos cuya definición aun no está consensuada y por tanto pueden provocar equívocos en cuanto a la definición que acabamos de realizar. Nos referimos a términos que pueden considerarse similares e incluso sinónimos como infopolítica, democracia digital, política digital, democracia electrónica, e-democracia, cibergobierno, política en red, etc.



\par 

El término web 2.0 está estrechamente vinculado a las redes sociales digitales y a la participación activa de los usuarios en la construcción colaborativa de comunidades. El adjetivo \emph{``electrónico''} en cambio, en opinión del autor es utilizado con demasiada frecuencia en diversos campos y no sólo para vincularlo a aparatos electrónicos, sino en sustitución del adjetivo \emph{``digital''} que en algunos contextos sería más adecuado (libro electrónico versus libro digital  \Parencite{792}, firma electrónica, etc.) En cualquier caso, aparatos electrónicos tan frecuentes hoy en día como relojes, radios, calculadoras y llaves poco o nada tienen que ver la definición de política 2.0 que hemos realizado. Y el adjetivo digital tampoco parece encajar mucho mejor. Los prefijos \emph{``info-''} y \emph{``ciber-''} tampoco parecen resaltar los aspectos más importantes de la definición. Respecto al prefijo \emph{``e-''} como acrónimo de electrónico e \emph{``i-''} como acrónimo de información, su pertinencia ya ha sido aclarada.



\par 

El término poĺitica en red resulta bastante adecuado pero podríamos considerarlo una generalización del término política 2.0 puesto que no hace referencia directamente al uso de las nuevas tecnologías. A pesar de ello, tanto la política en red en general como la política 2.0 en particular es de esperar que minimicen o eviten por completo la aparición de relaciones jerarquicas y promuevan las relaciones directas entre un amplio número de sus componentes.



\par 

Este concepto es el que se ha dado en llamar democracia directa  \Parencite{799}, que en el caso de la política 2.0 sería la denominada democracia directa electrónica  \Parencite{800} (nótese el uso poco conveniente del adjetivo electrónica también en este caso). Llegados a este punto, es importante resaltar que la democracia directa en una sociedad es una utopía puesto que no es posible que todos los ciudadanos sean capaces de ejercer sus derechos (niños, personas mayores, enfermos, discapacitados) y por tanto siempre es necesario mantener cierto grado de democracias representativa  \Parencite{801}. Por tanto, digamos que política 2.0 es un término que resulta más pragmático y está abierto a distintos equilibrios entre la democracia directa y la democracia representativa.



\par 

Por último, aquellos lectores a los que les parezca inadecuado el adjetivo 2.0 por el mero hecho de que se utiliza con demasiada profusión (turismo 2.0, empresas 2.0, universidad 2.0, finanzas 2.0, ciudadano 2.0, parlamento 2.0, propaganda 2.0) y en algunos casos sin demasiado rigor, les pedimos que ignoren los usos inadecuados de este y otros términos y se centren en los usos adecuados (copyright 2.0  \Parencite{605}) puesto que en la mayoría de los casos se utiliza para resaltar el uso de las redes sociales digitales como vehículo para revolucionar realidades ya existentes que se están viendo afectadas por la participación y la colaboración activa de las personas en distintos ámbitos de producción, es decir, de la misma forma que se está utilizando el adjetivo 2.0 en el presente artículo pero aplicado al ámbito de la construcción social.



\par 

En cualquier caso, política 2.0 no es más que un término al que de forma más o menos arbitraria hemos decidido asignarle un significado. Quien no esté de acuerdo con esta decisión es libre de usar otro término.









\section{Algunas iniciativas locales}

\par 

A continuación de presentan los grupos, colectivos, proyectos e iniciativas relacionadas con política 2.0 que el autor ha estudiado. Al ser un estudio personal (en la mayoría de los casos fruto de la participación en dichas iniciativas) todos tienen un vínculo con el País Vasco.



\subsubsection{Politika 2.0}

\par 

\url{http://politika20.nireblog.com/}



\par 

Es punto de encuentro, reflexión y acción de una serie de personas 2.0, principalmente vascas, que intentan promover cambios en lo que se define como Política, hacia modelos más participativos y cercanos a una ciudadanía activa.



\par 

Los proyectos más significativos que este grupo a llevado a cabo son los siguientes: Jornada informativa en el Parlamento Vasco, 119 segundos, 11 minutu, presentación en el Parlamento Europeo.



\subsubsection{Colabora en nuestras ciudades}

\par 

\url{http://colaboraenred.com/}



\par 

Se trata de un proyecto de colaboración ciudadana, que busca la implicación de las personas y sus problemas, conocimientos e ideas, y que culminará en un evento o reunión donde todas aquellas personas que lo deseen podrán participar con sus problemas, con sus soluciones, tecnológicas o no, si las tienen, donde otras personas les aportarán también las suyas y donde quizás se planteen nuevas ideas a desarrollar o retomemos las existentes para mejorarlas.



\par 

Las ciudades donde se ha establecido este grupo hasta la fecha son Bilbao, Vitoria-Gasteiz, Donostia-San Sebastián y Getxo. Una vez recopiladas una serie de ideas, el objetivo de los dinamizadores de esta iniciativa es presentarse formalmente en cada ayuntamiento y ofrecer formas de colaboración.



\subsubsection{Ciudadanos 2010}

\par 

\url{http://www.ciudadanos2010.net/}



\par 

Es otra experiencia de democracia participativa en el que participan decenas de municipios españoles y su objetivo es ofrecer a los ciudadanos una herramienta para comunicarse directamente con su consistorio, debatiendo y comentando las aportaciones del resto de vecinos. Este proyecto ha sido uno de los primeros que ha permitido a un elevado número de municipios españoles tener una primera experiencia de las posibilidades de interacción que permiten las nuevas tecnologías entre el ayuntamiento y sus habitantes. 



\subsubsection{Parlio}

\par 

\url{http://parlio.org/}



\par 

El objetivo de esta iniciativa es hacer la actividad parlamentaria más transparente, filtrando, reordenando y reestructurando la infromación ofrecida por el Parlamento Vasco desde su web, cuya usabilidad deja bastante que desear. De esta forma, en lugar de solicitar al parlamento o a alguna otra institución que ponga en marcha un proyecto de análisis de la información contenida en la web del parlamento y posteriormente lleve adelante una remodelación y modernización de la misma, cuatro amigos se reunieron durante un fin de semana y en tan sólo 48 horas crearon una nueva página web a la que llamaron Parlio y que se alimentaba automáticamente de la página original pero reestructurando la información de una forma mucho más clara y mostrando los datos interesantes para los usuarios de la web de forma clara (utilizando diagramas, etc.).



\par 

El éxito de esta pequeña iniciativa fue mayúsculo puesto que demostraron que si las administraciones públicas hacen pública la información que manejan (aunque sea en crudo), la iniciativa ciudadana puede hacer uso de dicha información como más le convenga. Este concepto se conoce con el nombre de Datos Abiertos  \Parencite{820} y en los últimos tiempos está tomando cada vez más fuerza puesto que parece lógico que la información y los contenidos creados con fondos públicos sean abiertos y accesibles a toda la sociedad. 



\subsubsection{Ezker Batua Berdeak}

\par 

\url{http://programa.ezkerbatua-berdeak.org/}



\par 

Esta iniciativa la llevó adelante un partido politico del País Vasco antes de las elecciones locales del año 2008 y su objetivo fue dar la posibilidad a sus militantes y simpatizantes, así como a quien quiera que estuviera interesado en mejorar el programa electoral de dicho partido. Para ello hicieron uso del mismo software libre con el que funciona el agregador social meneame.net, de forma que en lugar de noticias, los participantes proponían, catalogaban y publicaban puntos a incluir en el programa electoral.



\par 

Una anécdota no exenta de cierta paradoja fue el hecho de que otro partido (PSEE-EE) propuso en esta herramienta abierta la posibilidad de enviar todo su programa electoral como propuesta, cosa que Ezker Batua Berdeak declinó. 



\subsubsection{Neskateka}

\par 

\url{http://neskateka.net/}



\par 

Neskateka es un proyecto que surgió del departamento de igualdad del Ayuntamiento de Laudio/Llodio y su objetivo es empoderar a las mujeres del municipio que están agrupadas en asociaciones  \Parencite{819}. Además de reuniones presenciales con dichas mujeres y otros eventos públicos, esta iniciativa produjo como resultado un mapa de veintidos espacios inseguros, con accesos complicados o con deterioro y falta de mantenimiento importante en Laudio. Este mapa resultante del proceso que recoge y ubica las zonas menos seguras de la localidad fue buzoneado por todos los domicilios de la localidad dando a conocer la iniciativa Neskateka apoyada por el ayuntamiento. Además, también se plantearon propuestas de actuación para mejorar los puntos inseguros.



\subsubsection{arreglaMicalle}

\par 

\url{http://arreglamicalle.com/}



\par 

Es una herramienta web que permite a cualquier ciudadano dar parte de desperfectos y problemas en las calles de su ciudad. El objetivo de la micro-empresa que lo ofrece es llegar a un acuerdo con distintos ayuntamientos para informarles periódicamente de los reportes recibidos por los ciudadanos, así como darles la oportunidad de informar en la web de las actuaciones realizadas.



\subsubsection{Queremos software libre}

\par 

\url{http://www.queremossoftwarelibre.org/}



\par 

Esta iniciativa parte de un conjunto de personas preocupadas por que el Gobierno Vasco destine el dinero público en promover y fomentar el uso de software libre y estándares abiertos en las instituciones públicas vascas y en todos los ámbitos de la sociedad vasca. Para ello, antes de las elecciones locales de 2009 este colectivo puso en marcha una recogida de firmas en apoyo de un manifiesto con las exigencias indicadas y también realizó una encuesta a los candidatos a \emph{Lehendakari} (presidente local)  \Parencite{821}.



\subsubsection{Sindominio}

\par 

\url{http://sindominio.net/}



\par 

Sindominio es un conjunto de servidores autónomos que opera en el Estado español, y acaba de cumplir diez años de andadura y crecimiento constante. Sus participantes son varios centenares de activistas y \emph{hacktivistas}, albergando y gestionando los sitios web de incontables colectivos y movimientos sociales. Utiliza únicamente software libre, trabaja de forma asamblearia (mediante una lista de correo que funciona como una asamblea virtual permanente) e incluso las labores de administración de las máquinas se realizan conjuntamente por varios usuarios de confianza de la comunidad  \Parencite{804}. En sus comienzos incluía un medio de comunicación independiente llamado ACP, que fue asimilado al proyecto Indymedia.



\subsubsection{Indymedia}

\par 

\url{http://indymedia.org/}



\par 

Indymedia o Independent Media Center (Centro de Medios Independientes en inglés), es una red global participativa de periodistas independientes que informan sobre temas políticos y sociales. Esta red fue creada en 1999 durante las manifestaciones contra la cumbre de la Organización Mundial del Comercio (OMC) en Seattle, Estados Unidos, y aunque oficialmente no aboga por ningún punto de vista en particular, está estrechamente relacionada con el movimiento antiglobalización. Indymedia utiliza un proceso de publicación abierto y democrático en el que cualquiera puede contribuir. De hecho uno de sus lemas más conocidos es el siguiente: ``No odies a los medios, conviértete en ellos'' (\emph{``Don't hate the media, become the media''} en inglés).



\par 

Tras el éxito de la primera web creada para cubrir la contracumbre de Seattle (su formato es lo que después comenzó a denominarse como \emph{blog}) y romper la manipulación mediática del evento, otros Centros de Medios Independientes fueron surgiendo a lo largo y ancho del planeta hasta completar los aproximadamente 180 actuales que se reparten por los 5 continentes (aunque la mayoría están ubicados en Estados Unidos y Europa). Toda la red de Indymedia es completamente abierta y recibe aportaciones de todo tipos de agentes sociales de base.



\par 

Todos los sitios web y los servidores de Indymedia funcionan con software libre, trabajan con formatos estándares y sus contenidos son libres. Otra de las características de esta red de centro de medios independientes es que sus servidores no guardan bitácoras sobre sus usuarios de forma que se trata por todos los medios técnicos y legales de minimizar la censura de cualquier tipo y maximizar la libertad de expresión.



\subsubsection{Hacktivistas}

\par 

\url{http://hacktivistas.net/}



\par 

Es una red de hacktivistas  \Parencite{823} que nace de la comunidad de hacklabs  \Parencite{824} y hackmeeting  \Parencite{825} de la península ibérica. Este comunidad creó en el año 2008 un espacio para coordinar sus acciones a nivel global, debatir estrategias, compartir recursos y sincronizar movimientos de creación y resistencia hacia una sociedad libre con unas tecnologias libres. Es por tanto un punto de encuentro y reflexión telemático que lleva adelante una serie de acciones físicas y/o virtuales siempre vinculadas a la tecnología y el uso social de las mismas.



\par 

Los proyectos más significativos que este grupo ha llevado a cabo son lo siguientes: defensa de la cultura y el conocimiento libre, campana informativa y acciones contra del denominado ``Paquete Telecom'' 6 de Mayo de 2009 del Parlamento Europeo, creación de ``La lista de Sinde''. 



\subsubsection{Partido Pirata}

\par 

\url{http://www.partidopirata.es/}



\par 

Es un partido político fundado en Suecia en el año 2006 que busca la reforma de las leyes de propiedad intelectual, el respeto al dominio público y la promoción de la cultura copyleft. Uno de sus principios fundamentales es también la defensa de la privacidad tanto en Internet como fuera de ella. Para el resto de temas que no tienen que ver directamente con las leyes de copyright, las patentes de software y similares, no tiene una posición fijada ni pretende tenerla. Con los años, el Partido Pirata se ha ido extendiendo por Europa (Inglaterra, Francia, Alemania, Finlandia, etc.) y también por América (Canadá, Mexico, Chile, Argentina, etc.).



\par 

En las elecciones al Parlamento Europeo celebradas en 2009, el Partido Pirata sueco fue la quinta fuerza más votada, obteniendo el 7,13\% de los votos (un total de 226.000), por lo que hay un europarlamentario del Partido Pirata que defiende en esa institución los intereses de este partido. Ese mismo año, en las elecciones federales en Alemania, el Partido Pirata alemán obtuvo el 2\% de los votos (un total de 846.000), lol que da una muestra de la fuerza que este partido está obteniendo año tras año. Actualmente el Partido Pirata está presente en Europa (Inglaterra, Francia, Alemania, Finlandia, etc.) y también en América (Canadá, Mexico, Chile, Argentina, etc.).



\subsubsection{Partido de Internet}

\par 

\url{http://partidodeinternet.es/}



\par 

En una plataforma ciudadana nacida en 2008 en España para tratar de implantar la democracia directa electrónica cumpliendo con la legislación vigente (haciendo uso del DNI-e\footnote{Para más información sobre el DNI electrónico español, consultar \url{http://www.dnielectronico.es/}).} A través de Internet y varias herramientas tecnológicas (sitio web, listas de correo, foros, chat, redes sociales\ldots) los voluntarios de este partido registrado en 2010 colaboran telemáticamente para hacer realidad sus objetivos más inmediatos. Entre ellos, ofrecer una alternativa al bipartidismo del Estado español, promover la creación de listas abiertas y presentarse a las Elecciones Generales Españolas de 2012.



\par 

Lo más novedoso de este partido, aparte de ser un grupo totalmente abierto y participativo que hace un uso intensivo de software libre y formatos abiertos, es el sistema de decisiones internas que está desarrollando. Agora es un sistema de voto electrónico seguro por Internet que está siendo desarrollado por los colaboradores del Partido de Internet y que permitirá a cada usuario elegir el grado de democracia representativa que desea (a este sistema se le conoce también con el nombre de democracia líquida  \Parencite{826}). En un extremo estaría aquel usuario que no delega su voto nunca y decide por sí mismo cada una de las decisiones que se le presentan (coincidentes con las votaciones de los diputados del partido en el congreso). En el otro extremo estaría el de aquel usuario que delega su voto incondicionalmente a una única persona o grupo y no participa de ninguna otra forma (logrando así el mismo nivel de democracia representativa que existía hasta ahora).Y por último estarían todos aquellos usuarios que delegan su voto en una serie de personas y grupos para distintos temas pero que además ante ciertos temas de su interés ejerce su derecho a voto directamente. Las personas pueden delegar su voto en cadena, de forma que si un usuario confía en otro usuario delegando su voto en él, si resulta que este usuario a su vez ha delegado su voto en un tercero, será este el que estará votando por él mismo y también por toda la gente que directa o indirectamente ha delegado su voto en él.



\par 

Los diputados del Partido de Internet irán votando en el congreso directamente lo que los votantes vayan decidiendo finalmente a través de Agora. Si Partido de Internet tiene tres diputados, un tercio de los votantes ha votado sí, otro tercio ha votado no y el otro tercio se ha abstenido, un diputado votará sí, otro no y el tercero se abstendrá. De esta forma se trasladará de la forma más inmediata y fiel posible (en función del número de diputados y los resultados de cada votación) la voz del pueblo en el congreso.



\subsubsection{Irekia}

\par 

\url{http://irekia.euskadi.net/}



\par 

Es una iniciativa del Gobierno Vasco que pretende ser la aplicación que los dirigentes vascos hacen del llamado gobierno abierto  \Parencite{802}. Los tres pilares de Irekia (significa ``abierto'' en euskera) son la transparencia, la participación y la colaboración  \Parencite{803}. Este sitio opera con software libre y sus contenidos tienen una licencia libre. Ante iniciativas como esta, surge la duda de hasta qué punto el \emph{open government}, mediante la transparencia y la participación que preconizan no son más que un nuevo y atractivo envoltorio para gobiernos que funcionan exactamente igual que el resto.









\section{Conclusiones}

\par 

Tras el al análisis realizado sobre el concepto de política 2.0 y de los grupos e iniciativas estudiadas entorno a las innovadoras formas de hacer política a través de las nuevas tecnologías, se pueden extraer algunas conclusiones:



\subsection{Características comunes}

\par 

Debido a las características inherentes a las nuevas tecnologías (cuasi-ubicuidad, mayor inmediatez, tendencia a la topología en red, etc.), las propias iniciativas surgidas en este contexto (no sólo las de carácter político) muestran un conjunto de tendencias que van siempre en la misma dirección:



\begin{description}

\item[Mayor dinamismo]Tanto la interfaz de los sitios web como la forma y el modo de intercambiar la información entre las personas y las herramientas cambia con facilidad, lo que abre la posibilidad de hacer cambios en las formas de trabajar y colaborar con facilidad y rapidez.

\item[Menor formalismo]Los procesos burocráticos en los ambientes dinámicos no suelen ser muy apreciados y este no es ninguna excepción. Los formalismos en los grupos descritos son los imprescindibles y en algunos casos los grupos son abiertamente informales y enemigos de burocracia alguna. Los partidos políticos son probablemente los que más formalismos y burocracia utilizan.

\item[Menor jerarquía]En los grupos analizados las capaz jerárquicas son débiles y cambiantes y en algunos casos completamente inexistentes. Los partidos políticos son en este caso también los que tienen una jerarquía más marcada.

\item[Mayor emergencia  \Parencite{805}]A pesar de no existir muchos formalismos, ni jerarquías, el grupo necesita coordinarse para realizar las distintas tareas con el fin de alcanzar los objetivos que el propio grupo se marque así mismo, por lo que suelen surgir procesos de autoorganización en los que surgen estructuras más o menos complejas a partir del trabajo en común. Un ejemplo de la emergencia de estructuras de autoorganización es el de Wikipedia, con sus protocolos de trabajo, roles de colaboradores, normas de funcionamiento, etc.  \Parencite{613}

\end{description}

\subsection{Tipos de grupos}

\par 

Respecto a los tipos de iniciativas presentadas, se pueden clasificar en tres grandes grupos según sus objetivos y el tipo de sus integrantes:



\begin{description}

\item[Top-down (de arriba a abajo)]Algunas de las iniciativas presentadas parten de los actores políticos tradicionales (gobiernos locales, ayuntamientos, partidos políticos) y se acercan a los ciudadanos a través de las nuevas tecnologías en general y de las redes sociales en particular. Su objetivo es sobre todo hacer más transparente la gestión institucional y de forma muy limitada y controlada permitir también ampliar un poco las formas de participación de los ciudadanos. Este es el caso de la iniciativa llevada a cabo por Ezker Batua Berdeak, del proyecto Ciudadanos2010, del grupo Neskateka y de la iniciativa Irekia del Gobierno Vasco.

\item[Bottom-up (de abajo a arriba)]Partido Pirata, Partido de Internet, arreglaMicalle, Ciudadanos en red, Parlio y QueremosSoftwareLibre en cambio son iniciativas que parten de los ciudadanos, pero a pesar de ello, las instituciones gubernamentales son la pieza clave en este tipo de grupos, y de ellas depende por completo la continuidad de la participación de los ciudadanos.

El grupo Politika 2.0 es algo distinto puesto que está compuesto por ciudadanos de a pie y de políticos (e incluso de activistas que no están directamente interesados en las instituciones), por lo que podría clasificarse en cualquiera de los tres apartados, aunque teniendo en cuenta la mayoría de sus actividades y proyectos, parece más adecuado incluirlo en el grupo bottom-up.

\item[Autónomos]Los grupos restantes (Indymedia, Hacktivistas y Sindominio) también parten de la gente de la calle, pero tienen la particularidad de ser autónomos, de mantenerse completamente independientes respecto a las instituciones políticas ordinarias y los demás agentes de poder (medios de comunicación, empresas, lobbies, etc.). Esto no quiere decir que no pretendan influirlos, ya que, al ser grupos políticos, sus componentes tienen sin duda el objetivo común de transformar la sociedad de la que forman parte, incluidos dichos agentes. La diferencia es que en lugar de pretender ayudar o mejorar las instituciones del estado o depender de ellas para que los cambios tengan lugar, estos grupos del ámbito libertario hacen una apuesta por la horizontalidad, el asamblearismo y la autogestión, ejerciendo su derecho a adoptar acuerdos y ejecutarlos, construyendo sociedad desde la mismísima base social frente a cualquier estructura acumulativa de poder (como el propio estado).\end{description}

\par 

Como conclusión final a este trabajo, es importante dejar claro que en la actualidad se están dando grandes cambio prácticamente cada día en el punto de contacto entre la política y las nuevas tecnologías, por lo que este estudio debe tomarse como una recopilación de algunas tendencias detectadas en los últimos años por el autor. La información que contiene este artículo no es más que la punta del iceberg de los profundos cambios que se están dando en la actualidad en diversos lugares y que sin duda se seguirán dando y extendiendo en el futuro próximo. Por tanto, no es posible hacer un análisis completo y sistemático en medio de toda una revolución, pero puede que la información aquí recopilada puedan servir como material para otros análisis futuros.






