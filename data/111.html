<html><head></head><body><p>El movimiento del Software Libre ha sido visto tradicionalmente como un movimiento eminentemente técnico, orientado a la creación de un cuerpo común de conocimiento expresamente enfocado hacia la operación de sistemas de cómputo. Nosotros lo presentamos, en cambio, como uno de los detonantes, y como uno de los casos más claros de éxito, de los movimientos de Cultura Libre.
<p>Exploramos cómo el planteamiento del movimiento del Software Libre, nacido como movimiento ideológico a mediados de los 1980, corresponde con la lógica histórica del desarrollo científico a lo largo del crecimiento de la humanidad, y se nos presenta como un mecanismo congruente con el mecanismo del desarrollo científico y tecnológico que la ha impulsado a lo largo de milenios de civilización. Abordamos cómo la propuesta ideológica del Software Libre es exportada y ampliada otras areas del conocimiento humano generando una cascada de ideas inovadoras que hacen énfasis en la producción de conocimiento.
<p>Revisamos además algunos ejemplos, anclados en la sociedad del conocimiento, en que a través del Software Libre podemos desarrollar características básicas de la libertad en una sociedad moderna y democrática: La confiabilidad, la privacidad, el anonimato, las libertades individuales.


<h1>Introducción</h1>
<p>
Hoy en día, hablar del Software Libre es ya un tema común, y si bien hay muchos puntos que no son del todo comprendidos por los actores externos, las nociones que definen al movimiento han sido en buena medida adoptadas fuera del medio de los <em>hackers</em>[fn]Cuando un programador –o en general un entusiasta del cómputo– menciona a los hackers, no lo hace en el sentido distorsionado y sensacionalista de esta expresión que nos han impuesto diversos medios, haciendo referencia a un intruso o delincuente digital. Un hacker es quien conoce los secretos de su sistema, y es capaz de programarlo (y goza haciéndolo) para lo que sea que lo requiera [bib]8[/bib], [bib]19[/bib].[/fn] del cual se originó.

<p>
En este trabajo buscamos proyectar las ideas básicas del Software Libre hacia todos los campos del saber humano, generalizando al movimiento y presentándolo como un modelo mucho más genérico de producción de conocimiento. Analizamos también cómo es que este modelo va impactando otras áreas del saber, y proyectamos el impacto que puede tener en la construcción de una sociedad más democrática, igualitaria e incluyente.




<h2>La historia del conocimiento</h2>
<p>
El conocimiento puede ser entendido como “el estado de quien conoce o sabe algo” y conocer (del latín <em>cognoscere</em>) es entendido como el “el ejercicio de las facultades intelectuales la naturaleza, cualidades y relaciones de las cosas”[bib]37[/bib]. Así pues el conocimiento y la capacidad de conocer es parte de la naturaleza humana que puede ser entendida como un fenómeno psicológico o índole social.

<p>
Desde el primer punto de vista psicológico el aprendizaje da cuenta del conocer, pero siempre el eje es el trabajo cognitivo y no tenemos evidencia de ese conocimiento hasta que el individuo da evidencia conductual de él o decide compartir este aprendizaje. Es en este punto que lo aprendido se socializa y le permite a los otros negociar significados y apropiárselo, pasando de la interacción social a la estructura individual, así pues un aprendizaje de uno puede dar origen a una idea en otro.

<p>
Durante milenios la función simbólica del lenguaje (lenguaje oral) fue el medio transmisor de las ideas, que vinculados con el continuo intercambio tecnológico[bib]38[/bib] entre los grupos humanos han perpetuando ideas y generando especializaciones con cada nuevo eslabón. Conocer y distribuir este conocimiento es una forma natural de la humanidad, similar al efecto de la polinización de las abejas al realizar su actividad cotidiana.

<p>
Con la especialización en la producción de conocimiento, los tiempos en que los grupos e individuos deben invertir para generar aportes novedosos se amplió considerablemente. Así que la sociedad generó reglas para validar y compartir ese conocimiento, donde deberían ganar todos los actores involucrados y la humanidad en su conjunto.

<p>
El desarrollo científico es el mejor ejemplo de cómo la humanidad ha logrado acuerdos de procedimientos y de formas válidas de retribución en lo relativo a  la construcción del conocimiento. Es a partir de ella que surgen los conceptos que hemos heredado de derecho –moral y patrimonial– de autor, primer momento en que se equiparan los bienes intangibles con los tangibles.

<p>
Al llegar la masificación de la producción científica y literaria a través de la imprenta, resultó natural  que los autores cedieran algunos privilegios –por ejemplo, la exclusividad sobre de una obra– a los impresores a cambio de recibir otros por parte de ellos — el no reproducir una obra más allá de lo pactado, el compartir ganancias según acordado. Sin embargo, y nuevamente con el paso de los siglos, este pacto y este modelo han demostrado no llevar la velocidad de los cambios tecnológicos que hoy nos rigen.

<p>
Desde hace ya varias décadas, los mimeógrafos y –en general– las técnicas offset de impresión han puesto la imprenta al alcance de cualquiera. Ya no hace falta una inversión millonaria para montar un taller de impresión, reduciendo el nivel de entrada (y por tanto, el tributo que el autor debe pagar al prestador del servicio) para quien quiere masificar la distribución de una obra.

<p>
Del mismo modo que la imprenta de tipos móviles es vista como uno de los factores detonantes del fin de la edad media e inicio del renacimiento, el mimeógrafo permitió difundir las ideas base que llevaron a todas las grandes revoluciones de fines del siglo XIX e inicios del XX. No es casualidad que éstos hayan sido vistos como un peligro y como instrumentos que deben estar sujetos a un estricto control (llevando a incontables confiscaciones) por decenas de regímenes autoritarios.

<p>
Pero fue apenas hasta hace unos veinte años en que llegó el último y mayor parteaguas en lo relativo a la masificación: El que amplios segmentos de la sociedad tengan acceso a redes de cómputo, con capacidad de reproducir cualquier obra producida por la humanidad, sin degradación de calidad ni inversión de insumos.

<p>
Una interesante consecuencia de la distribución de obras de conocimiento o arte a través de medios digitales es la granularización y extensividad de las mismas — La unidad a transmitir de persona a persona deja de ser el libro o el álbum, para convertirse en el artículo o la pieza musical específicas. Llega un momento en que la idea vale más que el producto — A pesar de que el medio físico que sustenta a la obra tiene un valor intrínseco[fn]Este valor es el nos explica por qué sigue siendo importante para muchas personas comprar un disco a pesar de tener ya todas sus canciones o un libro que ya habían leído en formato digital. El medio físico sigue atrayéndonos, por ejemplo, gracias a las obras adicionales que éste incluye, por la capacidad de asignarle un peso emocional, o sencillamente por la conveniencia de tener la obra en un medio más cómodo.[/fn]. En este sentido, muchos productos se desdibujan para convertirse en compilaciones de obras menores.

<p>
Además (claro está, de una manera que depende de la naturaleza de la obra), si bien la obra puede reproducirse sin degradación de calidad, agregamos la posibilidad de distribuir la obra agregándole comentarios o corrigiendo diversos aspectos; la digitalización del conocimiento nos lleva a la difusión de obras maleables y extensibles. Más aún, hecho que se evidencía principalmente en diversas áreas artísticas, esto crea además el potencial de incluir directamente una o varias obras dentro de creaciones independientes sin una excesiva inversión de esfuerzo, reinterpretando y expandiendo lo logrado por el autor. Esto, a través de los denominados <em>remixes</em> (remezclas), <em>mashups </em>(integraciones), etcétera, con los que se va volviendo difusa la barrera autoral de cada uno de los implicados en agregar valor a una fracción de la obra.

<p>
Este desarrollo nos lleva a la necesidad de romper con el pacto que equiparaba los bienes tangibles con los intangibles, pues para todo propósito éstos han dejado de ser iguales. El conocimiento ya no requiere de inversión para su distribución, y han nacido diversos movimientos –cada uno desde su esquina virtual– que pugnan por devolver el dominio colectivo al conocimiento.




<h2>El cómputo como expresión del conocimiento</h2>
<p>
Los diferentes grupos que forman parte del movimiento del <em>Software Libre</em> se autodefinen por sobre de todo en base a las Cuatro Libertades, formalizadas en 1986 por la Free Software Foundation [bib]9[/bib]. Si la licencia bajo la cual determinado programa es distribuído cumple con los siguientes cuatro puntos, puede ser universalmente aceptado como Software Libre:

<ol>
	<li>Libertad de uso para cualquier fin, sin restricción </li>
	<li>Libertad de aprendizaje </li>
	<li>Libertad de modificación/adecuación</li>
	<li>Libertad de redistribución</li>
</ol>
<p>
Claro está, si bien estos cuatro puntos dan coherencia a un amplio tejido social de movimientos cercanos, a lo largo de los últimos 20 años han surgido varias corrientes que explicitan varios puntos adicionales [bib]10[/bib], [bib]11[/bib].

<p>
El Software Libre no es, como muchos podrían suponer, un invento de los 1980. Durante las primeras décadas del desarrollo del cómputo, prácticamente la totalidad del software disponible era libre, si bien no había conciencia de ello. Era sencillamente lo natural; si bien el <em>trabajo invertido</em> en el desarrollo de soluciones era escrupulosamente cobrado –y dado lo restringido que era a esas alturas el campo, <em>muy</em> bien cobrado– por individuos o corporaciones, el <em>producto</em> resultante (cada uno de los programas) era entregado a quien había contratado su desarrollo <em>completo</em>, acompañado por su código fuente. Esto era, claro, necesario dada la dinámica del cómputo en esa época: Si bien un centro universitario, una rama del ejéricto o una gran empresa podían comprar una computadora de una marca conocida, lo más común es que la modificaran para cumplir con sus necesidades — Sí, que modificaran al <em>hardware</em> mismo, que le adecuaran interfaces no planeadas en el diseño original, que le conectaran unidades de almacenamiento o despliegue, etcétera.

<p>
Conforme el juego de abstracciones que conforman a un sistema de cómputo se ha ido haciendo más complejo, resultado claro del aumento de las capacidades de los equipos, los programadores han ido requiriendo menos del contacto con los <em>fierros</em>. Pero sí, en las décadas de 1950 y 1960, una computadora era entregada completa — Código fuente del sistema operativo, esquemas eléctricos de conexiones, todo lo necesario para poder... Usarla. Mucha gente dentro del movimiento del Software Libre se detendría aquí para llamar la atención hacia el hecho de que los usuarios de antaño esperaran como algo natural el recibir el código fuente, siendo que hoy en día, para prácticamente todos los sistemas que encontrarán en el mercado esta afirmación sería risible. Nuestra intención, sin embargo, va más allá: Es tomar al código fuente como una herramienta de expresión entre humanos, como una herramienta que permite formalizar y transmitir conocimientos, de una manera análoga a la notación utilizada en las matemáticas.

<p>
La noción de que los programas de cómputo son expresiones creativas y, por tanto, deberían ser protegidas por la garantía de libertad de expresión fue elaborada por primera vez por Phil Salin en 1991 [bib]303[/bib], y sigue siendo uno de los principales argumentos de varias de las luchas que en secciones posteriores detallaremos. En las conclusiones de su texto del 2004 [bib]188[/bib], Coleman hace mención a la clara tensión que existe entre los derechos de expresión y la propiedad intelectual. Las leyes de derechos de autor necesariamente limitan la difusión y el uso de las ideas que <em>pertenecen</em> a otros, y si bien el razonamiento detrás de esta censura efectiva indica que es por un bien mayor (el de fomentar una mayor creación a través de los incentivos económicos), el florecimiento de la producción generada por los movimientos de conocimiento libre demuestra que, al menos con las condiciones favorables a la circulación del conocimiento imperantes el día de hoy, es hora de reevaluar el contrato social que ha permitido etiquetar como propiedad a los bienes intangibles.




<h2>¿Qué es una sociedad democrática?</h2>
<p>
A lo largo de este texto, sostenemos que los movimientos relativos a la cultura libre han apuntado históricamente hacia la creación, promoción y desarrollo de una sociedad democrática. Resulta fundamental en este momento definir a qué nos referimos con este término.

<p>
Mucha gente relaciona el concepto de <i>democracia</i> directamente con el de votaciones para elegir la forma de gobierno — Esto no es más que una expresión (posiblemente la más limitada y de estrechas miras) de su significado. Las sociedades que equiparan <i>democracia</i> con <i>elecciones</i>, limitando la acción de la sociedad a meramente el ámbito electoral y creando una muy reducida clase política que es, para efectos prácticos, la única que puede verdaderamente llamarse <i>ciudadana</i> se terminan convirtiendo en <i>Estados sin ciudadanos</i>. Citando a Esteban Castro [bib]320[/bib]:

<blockquote><p>
Desde otro ángulo, la evolución a largo plazo de la ciudadanía occidental se ha caracterizado en términos generales por una expansión cualitativa y cuantitativa, aunque este proceso haya sido accidentado y también sujeto a tendencias regresivas. Si hablamos en términos generales, en la época moderna ser un ciudadano evolucionó de ser un “burgués” (hombre, jefe de familia y propietario) en las ciudades europeas medievales, a convertirse en un miembro individual (siempre hombre y propietario) de un Estado nación hacia finales del siglo XVIII, con la Revolución Francesa. Posteriormente, durante los siglos XIX y XX se desarrollaron formas cada vez más incluyentes de ciudadanía (siempre en el marco del Estado nación), que involucraron la expansión formal de la ciudadanía a las mujeres y a las mayorías no propietarias (aunque siempre se excluyó a amplios sectores de la población, a menudo por motivos étnicos). En tiempos más recientes, hemos sido testigos de la reaparición de formas tradicionales de ciudadanía así como también del surgimiento de nuevas formas cuya tendencia es trascender las fronteras de los Estados nación, como es el caso de las ciudadanías “pos-nacional”, “trasnacional”, “cosmopolita”, “mundial”, o “global”. Por lo tanto, en una perspectiva de largo plazo, puede decirse que como tendencia general, la membresía formal de los sistemas de ciudadanía se ha ido expandiendo hasta incorporar –tomando una frase de Norbert Elias– a “números siempre crecientes" de seres humanos.

<p>
(…)

<p>
De manera comprensible, la aplicación mecánica del concepto de ciudadanía a las experiencias de países no europeos es aún más problemático. Por ejemplo, ¿qué quiere decir ser ciudadano en América Latina, o mejor dicho en cada uno de sus países y regiones? Algunos autores han conceptualizado el caso de los países latinoamericanos como una situación de “Estados sin ciudadanos”, en donde el desarrollo de los Estados nación no tuvo correspondencia con la formación de una ciudadanía que pudiera dar base legítima al ejercicio del poder político. Otros se han referido a la existencia de “ciudadanos imaginarios”, en relación a los limitados intentos, a menudo artificiales, de trasplantar las instituciones de la ciudadanía liberal (particularmente la propiedad privada) a países como México, nación que tenía tradiciones indígenas y españolas muy bien establecidas de propiedad colectiva de los bienes naturales (tierra, agua, bosques).

</blockquote>
<p>
A través de los procesos relativos a la apropiación colectiva del conocimiento que hemos descrito hasta el momento, y a través de los movimientos y valores éticos que describiremos en las siguientes secciones, se crea una <i>verdadera</i> vida democrática — Los individuos que han tomado conciencia de estas posibilidades y se han tornado en creadores van forjando sociedades con una elevada dosis de conciencia de participación activa en sus procesos políticos, y que frecuentemente pugna por impulsar dichos procesos hacia la sociedad en general. Una sociedad democrática es aquella que se apropia de valores como la autoregulación, que define sus propias normas de gestión.

<p>
Mientras que, en el trabajo antes citado, Esteban Castro estudia cómo la capacidad ciudadana del individuo se ha ido reduciendo conforme se van cercando y privatizando los bienes comunes naturales y culturales, nosotros centramos nuestro análisis en el proceso interno: Cómo el participar en comunidades de creación colaborativa de conocimiento va llevando, sin proponérselo explícitamente (y mucho menos dando una orientación, aunque aparezca una orientación política emergente en las comunidades [bib]188[/bib]), a individuos más participativos de una vida política plena — Esto es, individuos que se apropian de los atributos que  la ciudadanía ha ido perdiendo en sus países.

<p>
El enfoque que en este trabajo damos a <i>construcción de una sociedad democrática</i> transita necesariamente por el del involucramiento político, por el de una sociedad participativa, con un significado similar al que Castro da a <i>ciudadanía</i>. Citándolo nuevamente (énfasis agregado):

<blockquote><p>
(…)nos aproximamos a la “ciudadanía” desde una perspectiva sociológica que enfatiza el <i>proceso de formación de la ciudadanía</i> más que la ciudadanía como estatus. Ante todo, ésta es un sistema de inclusión-exclusión que opera siguiendo criterios específicos para definir la <i>membresía de las personas dentro de una cierta comunidad política, incorporada la asignación de sus derechos y obligaciones</i>. Dicho proceso es altamente dinámico ya que la ciudadanía se desarrolla con el paso del tiempo en términos cualitativos y cuantitativos. Adopta una diversidad de formas en los distintos territorios, y se caracteriza por las permanentes contradicciones entre el estatus otorgado a las y los ciudadanos en el plano formal, y <i>el ejercicio efectivo, sustantivo de los derechos y obligaciones</i> que se les permite a los individuos en términos prácticos. En resumen, en este capítulo no estamos enfocando nuestra atención sobre la conexión entre ciudadanía y nacionalidad u otras formas de identidad política, sino más bien abordamos a la ciudadanía como el conjunto de relaciones sociales fundadas en el reconocimiento de los derechos y obligaciones mutuas que caben a los miembros de la sociedad en un plano de igualdad formal, y asimismo, enfatizamos las tensiones que surgen de las contradicciones entre esta igualdad abstracta del estatus formal de la ciudadanía y las asimetrías y desigualdades sociales concretas que caracterizan a los seres humanos reales.
</blockquote>
<p>
Para fines de nuestro análisis, la democracia conlleva y atraviesa naturalmente los diversos aspectos de autoregulación y gestión, y va de la mano de la participación social en los diferentes esquemas de producción[fn]Específicamente en lo tocante a la producción de cultura y conocimiento, mal llamados <i>propiedad intelectual</i>.[/fn], e incluso a la desaparición gradual de la barrera o distinción entre productores y consumidores en este sentido[fn]http://seminario.edusol.info/resena/psilab/2009/03[/fn].



<h2>Del software al conocimiento</h2>
<p>
Varios movimientos, de hecho, han nacido basados en conjuntos de premisas similares, y construyendo sobre del éxito de la convocatoria que el Software Libre introdujo:




Se entiende por cultura libre un movimiento social que promueve el desarrollo, y progreso de las <em>obras culturales libres</em> en un contexto de la sociedad digital  y de conocimiento; el movimiento de cultura libre surge en un contexto de leyes de derechos de autor con un énfasis restrictivo, que impiden o limitan ampliamente el desarrollo cultural en ámbitos digitales [bib]393[/bib].

<p>
Si bien la construcción de la Cultura Libre aun esta sujeta a interpretaciones de sus límites, existen  acuerdos mínimos que permiten hablar de la <em>cultura libre</em> como el libre acceso, creación, modificación  publicación y distribución de todo tipo de obras digitales. [bib]394[/bib] propone, de forma análoga a los convenciones que definen al  software libre:
<ul><li><strong>libertad de usar</strong> el trabajo y disfrutar de los beneficios de su uso,</li>
<li><strong>libertad de estudiar</strong> el trabajo y aplicar el conocimiento adquirido de él,</li>
<li><strong>libertad de hacer y redistribuir copias</strong>, totales o parciales, de la información o expresión, y</li>
<li><strong>libertad de hacer cambios y mejoras</strong>, y distribuir los trabajos derivados.</li></ul>
<p>Para posteriormente con [bib]395[/bib] plantear una definición oficial y consensuada de las obras culturales libres.

<p>
Hay una gran variedad de grupos que se identifican con la Cultura Libre, por lo cual nos limitaremos a mencionar algunos ejemplos sobresalientes.

<p>
Wikipedia nació con una idea que no es novedosa; ser una colección del conocimiento humano. Ya la biblioteca de Alejandría o los enciclopedistas del siglo XVIII lo habían intentado; el antecedente inmediato de la Wikipedia fue Nupedia [bib]304[/bib] (de marzo de 2000 a septiembre 2003), que surgió como una enciclopedia de libre acceso y redistribución que garantizaba la calidad de sus contenidos por medio de la revisión por pares. En los primeros 18 meses sólo se publicaron 20 artículos. En la búsqueda de nuevas fórmulas para involucrar a más personas en la producción de contenidos, se pensó que los usuarios de Nupedia crearán los contenidos que luego los editores y expertos revisarían, de esta forma Wikipedia inició trabajos rebasando rápidamente la velocidad de producción de Nupedia, dejándole con el tiempo inoperativa. En la edición en inglés el primer mes tenían 1,000 artículos; al primer año 20,000; en el segundo año 100,000 artículos, y al día de hoy, sobrepasa los 3,300,000. Del mismo modo, al igual que inició únicamente en inglés, otros idiomas no tardaron en arrancar, y actualmente hay más de 30 lenguajes de Wikipedia con más de 100,000 artículos[bib]743[/bib]. La gran revolución que generó el éxito de Wikipedia fue que ésta pasó de la idea de compilar el conocimiento revisado por pares expertos y disponible bajo un la licencia de uso que garantizara su libre redistribución, a una comunidad compuesta por visitantes, usuarios que contribuyen, bibliotecarios, burócratas y un comité directivo organizado en torno a una sociedad sin fines de lucro que se encarga del financiamiento del proyecto y la organización del encuentro anual de wikipedistas. [bib]36[/bib]

<p>
Wikipedia, así como todas las comunidades que van construyendo su acervo documental basados en el perfeccionamiento reiterativo y colectivo, constituyen un claro ejemplo de que <em>ningún conocimiento es acabado</em>[fn]http://seminario.edusol.info/resena/edzam/2009/03[/fn].

<p>
Pero no sólo el conocimiento formalizado puede ser compartido. En 2001 nació Creative Commons (CC), impulsada por el abogado estadounidense Larry Lessig. CC es una organización lidereada localmente en una gran cantidad de países por personalidades versadas en temas legales, buscando servir como punto de referencia para quien busque crear obras artísticas, intelectuales y científicas libres. CC ofrece un marco legal para que gente no experta en estos temas pueda elegir los términos de licenciamiento que juzgue más adecuados para su creación sin tener que ahondar de más en las áridas estepas legales; se mantiene asesorada y lidereada por el grupo de abogados, cuya principal labor es traducir y adecuar las licencias base de CC para cada una de las jurisdicciones en que éstas son aplicables. Un movimiento muy grande de creadores ha surgido alrededor de este modelo, y su propuesta ha sido acogida por una gran cantidad de sitios de alto perfil en la red. Si bien no todas las licencias de CC califican como cultura libre, al tener algunas que claramente sí lo son han ayudado fuertemente a llevar a la conciencia general estas ideas.




<h1>El Software Libre en tanto movimiento social</h1>
<p>
Típicamente, los seguidores de una ideología determinada no se limitan a impulsarla en el marco estricto en que se conformó originalmente, sino que terminan exportando sus ideas a otros campos, en un principio cercanamente relacionados, y cada vez englobando un mayor territorio. 

<p>
A algunos les parece dificil de entender, incluso un contrasentido, que los promotores del movimiento del Software Libre siempre han estado relacionados con la defensa de la privacidad y el anonimato. Estos dos puntos, si bien muy distintos, van claramente de la mano. Estos puntos resultan del interés de usuarios y desarrolladores por dos motivos principales: El reto técnico y el posicionamiento ético-ideológico. Veamos, pues, ambos casos.

<p>
La gente que más fuertemente se identifica con los principios que han impulsado al movimiento del Software Libre tiende también a tener una fuerte identificación con los diferentes movimientos que defienden las libertades individuales — Y si bien esto se nota en primer término en lo relativo a las libertades en línea, esta identificación llega a ámbitos muy variados.

<p>
Puede parecer contradictorio el que la misma gente que aboga fuertemente por la libertad y el acceso universal al código sea la misma que más fuertemente lucha por mantener –técnica y legalmente– el derecho a la privacidad y a la confidencialidad respecto a los datos y actividades personales, como lo veremos en esta sección. Probablemente los dos referentes más importantes en este rubro sean el Chaos Computer Club [bib]27[/bib], organización alemana fundada en 1981 que se autodescribe como <em>una comunidad galáctica de seres vivos, independientemente de su edad, sexo, raza u orientación social, que lucha a través de las fronteras en pro de la libertad de la información</em>, y la Electronic Frontier Foundation [bib]28[/bib], organización basada en los Estados Unidos que ha prestado asesoría o representación legal de diferentes maneras en casos relacionados con las libertades individuales en un mundo digital. En ambos casos, estas organizaciones nacieron muchos años antes de que los retos a los que se enfrentan se hicieran aparentes a la sociedad en su conjunto. Las investigaciones y las batallas legales que éstas han luchado a lo largo de sus décadas de existencia son, sin exagerar, causantes directas de que hoy en día tengamos libertad de expresión y acceso a la comunicación privada en Internet [bib]29[/bib].

<p>
Pero la identificación llega también por el otro lado — Varias prominentes organizaciones de la sociedad civil, han ido encontrando su afinidiad y la congruencia de sus objetivos con los diversos movimientos aquí mencionados. Organizaciones de todos los ángulos del espectro social han encontrado que la única manera de concentrarse en cubrir cabalmente sus objetivos es a través de las diversas expresiones del conocimiento libre<span class="diffchange">, y han encontrado una gran cantidad de elementos en común con el movimiento del Software Libre que los ha llevado a combinar sus experiencias y métodos, hibridizando los movimientos y creando una muy interesante sinergia</span>.

<p>
<span class="diffchange">El movimiento de Software Libre, argumenta [bib]188[/bib], es <em>políticamente agnóstico</em>. Mantiene que hay una relación no expresa y asimétrica entre el Software Libre y diversos movimientos políticos. Citando (traducido):</span>

<blockquote>
	<p>
	Es posible sospechar que el FOSS[fn]N. del T.: <em>Free and Open Source Software</em>, una manera de referirse a los diferentes sub-movimientos Software Libre / Open Source agrupándolos y obviando sus diferencias internas[/fn] tiene una agenda política deliberada, pero si se les pregunta, los desarrolladores de FOSS invariablemente responden con un "no" firme e inambiguo — típicamente seguido de un léxico preciso discutiendo la relación correcta entre el FOSS y la política. Por ejemplo, si bien es perfectamente aceptable tener un panel relativo al Software Libre en un congreso anti-globalización, los desarrollaodres de FOSS sugerirían que es inaceptable implicar que el FOSS tenga a la anti-globalización como uno de sus objetivos, o a cualquier otro programa político — Una diferencia sutil pero vital.
	
	<p>
	(…)
	
	<p>
	<span class="diffchange">Descrito simplemente, los reclamos políticos más allá del software limitan, ensucian y censuran la esfera de circulación de pensamiento, acción y expresión. Se siente que si el FOSS se dirige con fines políticos, contaminará la «pureza» del proceso de toma de decisiones. La afiliación política puede también desmotivar a algunas personas de participar en el desarrollo, creando una barrera artificial para entrar a esta esfera (…).</span>
	
</blockquote>
<p>
<span class="diffchange">Es sin embargo imposible disociar por completo a la política de cualquier movimiento social. Retomando el comentario hecho al presente artículo por Carolina Franco durante su desarrollo, todo movimiento social es inherentemente político, aunque esto se expresa en diversos vectores que muchas veces están claramente diferenciados de la política partidaria tradicional. Según la taxonomía presentada en [bib]601[/bib], basada en [bib]602[/bib], Franco define al movimiento de Software Libre como un <em>movimiento de reforma con alcance tanto grupal como individual, que utiliza métodos pacíficos de trabajo, y de rango global. ¿Por qué un movimiento de reforma y no radical? Porque busca cambiar algunas cosas en específico, que implican limitaciones</em>.</span>

<p>
Ejemplos de movimientos y organizaciones preexistentes que han encontrado alianzas naturales con el Software Libre son:

<ul>
	<li>Indymedia [bib]325[/bib] se autodefine como un <em>colectivo de organizaciones de medios independientes y de cientos de periodistas que ofrecen una cobertura de base y no comercial. Indymedia es una vía democrática  de medios de comunicación para la creación radical de narraciones verídicas y apasionadas.</em>. Indymedia cuenta con el apoyo y la cercanía de diversos miembros de grupos de desarrollo de Software Libre para la creación y administración del sistema de administración de contenido (CMS) Mir [bib]326[/bib] desarrollado a la medida de las necesidades particulares del flujo de información de los CMIs (Centros de Medios de comunicación Independientes), y es utilizado como base para más de 30 de ellos.</li>
	<li>Ya en 2002, el gobierno de la Comunidad Autónoma de Extremadura, España, puso en marcha al ambicioso proyecto de Linux Extremeño, conocido como gnuLinEx [bib]327[/bib]. A través de una distribución de GNU/Linux basada en Debian y preparada expresamente con las necesidades de la Comunidad Autónoma en mente lograron ahorros de licenciamiento tan amplios que permitieron dotar a las escuelas de una computadora por cada dos niños en edad escolar; gnuLinEx además dio el gran salto para convertirse de consumidor de tecnología en un fuerte promotor del desarrollo, creando a una plantilla de desarrolladores dedicados a mantener a la distribución al día, y contribuyendo de vuelta a los proyectos <em>padre</em>, principalmente Debian. La Junta de Extremadura ha patrocinado una gran cantidad de reuniones de desarrollo, generales y focalizadas a lo largo de todos estos años, incluyendo una importante inversión que permitió la celebración del DebConf (congreso anual de desarrolladores de Debian) en julio del 2009.</li>
	<li><span class="diffchange">La Fundación Heinrich Böll [bib]600[/bib] se constituyó en Colonia, Alemania, en 1986, en el seno del partido alemán Alianza 90/Los Verdes, y ha ampliado su ámbito de acción a diversos países de América Latina. La Fundación Böll centra su actividad en cuatro líneas: Ecología y desarrollo sostenible, derechos de la mujer y democracia de género, democracia y derechos humanos, y libertad de prensa y crítica pública. Organizan diversas actividades académicas y culturales, y son una prolífica editorial en estos temas; desde hace varios años se ha acercado a diversos activistas del Software Libre, dado que los temas que aborda éste movimiento son transversales a muchos de los que desarrolla ésta fundación, y desde hace varios años han publicado varios libros colectivos y multidisciplinarios en los cuales el punto de vista de las comunidades de Software libre tienen un importante peso.</span></li>
</ul>
<p>
<span class="diffchange">Ejemplos como los anteriores, claro está, hay muchos más, y estos deben ser vistos únicamente como una pequeña muestra</span>.




<h2>Posicionamiento ético-ideológico</h2>
<p>Hay una amplia gama de luchas que podrían parecer completamente laterales a la substancia de sus características básicas con las que se han identificado los grupos de desarrollo de software libre — en esta sección presentaremos algunos ejemplos. Estas luchas tienen frecuentemente importantes implicaciones sociales y/o políticas; estas luchas están frecuentemente relacionadas con la interpretación de diferentes actividades en el ámbito de los derechos civiles, como el derecho a la privacidad o la libertad de expresión.
<p>Un punto interesante a tomar en cuenta en relación a estas luchas es la manera en que el razonamiento relativo es frecuentemente presentado desde puntos de vista más cercanos a la lógica formal que a la lógica política desde la cual en muchos otros ámbitos pueden ser presentados. Esta característica ha llevado a resultados tan contundentes y sorpresivos como el caso de Bernstein contra el Departamento de Justicia de los E.U.A. [bib]328[/bib], que dictamina que el código es considerable como expresión de la creatividad humana y, por tanto, debe gozar de la protección de las garantías de libertad de expresión.


<h2>El reto técnico</h2>
<p>
La criptografía es la ciencia de cifrar o descifrar información. Es a través de la criptografía que podemos asegurar que en determinada comunicación tendrá las características básicas de lo que entendemos por privacidad: La <em>confidencialidad</em> (sólo el destinatario legítimo podrá conocer el contenido de determinado mensaje) y la <em>no-repudiabilidad</em> (un mensaje firmado criptográficamente no puede ser desconocido por su autor). 

<p>
La criptografía tiene un rol fundamental en cuanto a la relación de la defensa de la privacidad: Más que enfrentarse a la problemática legal de la protección a la privacidad (cosa que se complicaría tremendamente al entrar en juego las diferentes jurisdicciones legales en todo el mundo), resulta mucho más atractivo –y productivo– para un programador el implementar un algoritmo criptográfico. Y es aquí donde entra, en gran medida, el reto técnico como motivador: Como lo describe Linus Torvalds desde el mismo título de su libro <em>Just for Fun</em> [bib]25[/bib], probablemente el mayor factor que ha llevado al desarrollo del Software Libre es el <em>reto</em> que supone resolver un problema complejo e interesante.

<p>
La criptografía (y su contraparte, el criptoanálisis) son hoy en día consideradas sin lugar a dudas disciplinas científicas de pleno derecho, derivadas de las matemáticas. Esto, sin embargo, no siempre fue así — La criptografía tiene milenios de antigüedad, y el criptoanálisis tiene siglos, pero hasta mediados del siglo XIX no eran más que divertimentos o esquemas triviales basados en trasposiciones o substituciones [bib]22[/bib]. 

<p>
Ya en el siglo XX, ambas disciplinas –auxiliadas casi siempre de implementos mecánicos– jugaron papeles clave en las dos guerras mundiales. El matemático polaco Marian Rejewski, de 27 años, logró en 1932 romper –utilizando un enfoque meramente matemático– el código <em>Enigma</em> del ejército alemán [bib]26[/bib], logrando una ventaja clave para el desarrollo de la guerra, y abriendo formalmente este campo de las matemáticas. En 1976, Whitfield Diffie y Martin Hellman introdujeron la <em>criptografía de llave pública</em> [bib]23[/bib], dando a esta disciplina aplicabilidad práctica ya no sólo como juego o como herramienta para espías, sino para la vida cotidiana.

<p>
Es aquí donde entra el interés por resolver un reto interesante — El diseño y la implementación tanto de algoritmos criptográficos como de aplicaciones que los aprovechen, así como nuevos casos de uso donde puedan ser aprovechados, son campos de altísimo interés especialmente para matemáticos y científicos computacionales — Poblaciones que al día de hoy siguen teniendo una amplia representación dentro de la comunidad programadora. 




<h1>Conclusiones</h1>
<p>
A lo largo de este artículo presentamos algunos ejemplos ilustrando por un lado la importancia de la libertad del conocimiento, ya no sólo como una ventaja para el individuo o como una metodología de desarrollo, sino como un eslabón natural del proceso de descubrimiento, adecuación, descripción y modificación de su entorno que la humanidad ha realizado desde su aparición en la Tierra. Además de esto, ilustramos con un par de ejemplos el por qué este movimiento está firmemente basado en valores éticos y políticos que han creado una cultura sobre la compartición del conocimiento.

<p>
Este movimiento, particularmente en los últimos años, ha sido objeto de análisis –pero sobre todo, ha ido ganando simpatizantes– por parte de especialistas de diferentes ramos del conocimiento humano, que han ido ampliando y conocimiento estas nociones de libertad a sus distintas áreas disciplinarias. El impacto que pueda tener sobre de la producción de la humanidad toda a futuro apenas está comenzando a vislumbrarse.



</body></html>