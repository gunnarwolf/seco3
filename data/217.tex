\textbox{

\par 

La postura que ante las votaciones electrónicas han tomado diversos grupos relacionados con la creación y escrutinio de software y de procesos sociales ilustra muy bien varios de los puntos delineados en otros capítulos de la presente obra. 

\par El presente capítulo formaba parte originalmente del capítulo ``Software libre y construcción democrática de la sociedad'', e ilustra uno de los puntos y de las maneras en que las comunidades de creación de conocimiento tanto de seguridad en cómputo como de Software Libre han abordado un punto de gran importancia para la vida en sociedades democráticas actuales, insertándose en el entorno político imperante.

\par 

Hemos decidido, tanto por la extensión como por la relación de este tema con varios otros de los presentados en esta obra, hacer del presente apartado un capítulo independiente.



}

\par 

Los promotores de las diferentes vertientes del Conocimiento Libre son los primeros en recalcar los tremendos fallos –de concepción y de implementación– que hacen que las estaciones computarizadas de emisión y contabilización de votos sean, desde su planteamiento, una causa perdida  \Parencite{31} — Ninguna de las numerosas implementaciones a la fecha han salido airosas ante el escrutinio (incluso casual) de expertos en seguridad  \Parencite{32}, a veces con resultados verdaderamente nefastos  \Parencite{34},  \Parencite{35}. Los escrutinios generalmente han sido dirigidos por grupos de activistas independientes buscando señalar las deficiencias del proceso, con la muy notable excepción del ejemplo puesto por el Tribunal Superior Electoral de Brasil, al cual abordaremos más adelante.



\par 

Obviamente, estos resultados no son del agrado de las compañías que buscan vender máquinas supuestamente seguras, diseñadas ex-profeso para el conteo de votos. Se han dado a conocer incluso amenazas hechas contra dichos equipos de investigadores  \Parencite{33} por llevar a cabo estos análisis. En este caso, la demanda es que, en asuntos tan sensibles, relevantes y tan frágiles ante la intervención como la vida de una sociedad democrática, es sencillamente imposible asegurar los elementos básicos de confiabilidad y auditabilidad.



\par 

Cabe aclarar que la argumentación que haremos en este capítulo no pone en duda los \emph{fundamentos matemáticos} de diversos sistemas que serían aplicables a una votación electrónica, como las garantías descritas por  \Parencite{770}, sino su \emph{implementación} — Duda que se mantiene, como veremos más adelante, aún partiendo del supuesto de que el código está 100\% disponible para su escrutinio. Lo que es más, algunos de los mecanismos descritos por Ruiz pueden ser empleados en contra del sistema democrático\footnote{Tomo como ejemplo la prueba de la emisión de un voto basada en \emph{cero conocimiento}: Si bien el esquema sugerido permitiría que cada votante individual verifique que su voto haya sido tomado en cuenta, sin divulgar el sentido de los votos de los demás, esta propiedad sería ideal para la \emph{compra de votos}, tan tristemente común en nuestra América Latina. Si en una votación pudiera demostrar por quién voté, indudablemente habrá quien sepa exigirme le demuestre que voté en el sentido ``correcto''.}.



\par 

Diversos argumentos han sido esgrimidos a favor del voto electrónico, pero pueden ser resumidos en tres:



\begin{itemize}

	\item \textbf{Disminución de costos}: Un adecuado proceso democrático es caro. La papelería electoral debe ser impresa con mecanismos suficientes para asegurar su unicidad, deben proveerse mecanismos para garantizar que sólo los electores autorizados emitan su voto, y debe haber garantías de no manipulación para todos los componentes involucrados en el proceso. La automatización del proceso ayudaría a implementar estos candados a un menor costo.

	\item \textbf{Agilidad en la obtención de resultados}: No hay nada que genere mayor falta de confianza y suspicacia en los procesos que una demora en la publicación de los resultados. Se ha argumentado que a través del voto electrónico, los resultados pueden ser anunciados prácticamente de inmediato tras haberse cerrado la casilla.

	\item \textbf{Confiabilidad de los actores}: La experiencia de muchos países en torno a los fraudes electorales apunta dolorosamente a la falta de integridad de los actores involucrados en el proceso — Personas susceptibles ya sea a la compra de conciencias, a la extorsión, o directamente a la violencia física; si todo el proceso es controlado por computadoras, éstos factores deberían perder peso.

\end{itemize}

\par 

En las siguientes secciones analizamos por qué los tres argumentos caen ante un sencillo análisis.









\section{Disminución de costos}
\label{disminucion-costos}

\par 

La sociedad está acostumbrada a lidiar con los bemoles del voto \emph{tradicional}, utilizando al papel como su medio primario. Una crítica muy común a éstos procesos, especialmente en los países cuyas democracias no están bien consolidadas (y que por tanto, requieren de mucho mayor inversión tanto en la vigilancia como en la promoción de la participación de las elecciones) es el costo. Podemos tomar como un caso extremo a México, el sistema electoral más caro de América Latina  \Parencite{330},  \Parencite{761}: cada sufragio emitido tanto en las elecciones federales intermedias del 2009 como en las estatales del 2010 tuvo un costo superior a los 17 dólares, aunque hay estimaciones que lo llegan a ubicar en hasta 50 dólares, tomando en cuenta \emph{gastos ocultos}.

\par 

Como mencionamos anteriormente, un rubro que en el sin duda podrían presentarse importantes ahorros es en la generación, el manejo y la custodia del material electoral. Sin embargo, como queda demostrado tras el estudio realizado por Feldman, Halderman y Felten a las estaciones de votación Diebold AccuVote-TS  \Parencite{331}, las más difundidas en los Estados Unidos y que han sido responsables de la recopilación de votos de hasta uno de cada diez electores de dicho país, enfrentadas a un atacante con conocimiento técnico especializado, éstas máquinas presentan un nivel de confiabilidad ante ataques verdaderamente bajo, y permiten —requiriendo de un tiempo mínimo de acceso— la reprogramación resultando en resultados fraudulentos que serían prácticamente imposibles de lograr en una elección tradicional sin recurrir a métodos violentos.

\par 

Las vulnerabilidades descritas por Feldman, Halderman y Felten no son privativas a los equipos Diebold — En el sitio Web en el cual está publicado su artículo junto con un video de \emph{diez minutos} demostrando su ataque y una lista de preguntas frecuentes mencionan: (traducido)



\begin{quotation}
	\par 
	\textbf{¿Por qué estudiaron éstas máquinas Diebold? ¿Por qué no otras tecnologías para votos?}
	\par 
	Estudiamos estas máquinas porque son las que conseguimos. Si hubiésemos tenido acceso a otro tipo de máquinas, probablemente las hubiéramos estudiado.
	\par 
	\textbf{¿Son otras máquinas más seguras que las que estudiaron?}
	\par 
	No lo sabemos. Esperamos que así lo sean —las elecciones dependen ya de ellas— pero no hay suficiente evidencia para responder a esta pregunta
	\par 
	Un rastro impreso verificado por cada votante es la protección más importante que puede hacer más seguras a las máquinas de voto electrónico.
\end{quotation}

\par

Inclusive si el costo de adquisición o desarrollo de las urnas electrónicas fuera cero (que no lo es), si fueran suficientemente portátiles como para que su transporte a las mesas de elección no requiriera de logística especializada (que no lo son), y que fueran tan confiables como para no requerir personal especializado para darles soporte técnico en caso de fallo (que, nuevamente, no lo son), teniendo la evidencia de que su comportamiento puede ser modificado en tan sólo diez minutos, y agregando a este ejemplo muchos de los que serán expuestos en la sección \ref{experiencias-internacionales}, llegamos a la conclusión de que las urnas no pueden ser dejadas sin custodia \emph{ni por un periodo de diez minutos}. Una vez que un atacante logra la más trivial modificación en la operación de la urna, detectar el alcance de su manipulación y revertirla a un estado confiable es prácticamente imposible. En cambio, la papelería electoral sólo requiere de vigilancia desde el momento en que es impresa hasta que la elección es declarada válida y se ordena su destrucción — Un proceso de unas pocas semanas.

\par

En caso de un proceso electoral controvertido, como el sostenido en México en 2006, la situación resulta aún más complicada. Para asegurar una auditoría plena a una elección, es necesario conservar todo el material en un estado inalterado. Obviando la imposibilidad de auditar el estado interno de una computadora que abordaremos en la sección \ref{confiabilidad-actores}, esto requeriría que las urnas permanecieran sin ser utilizadas hasta que la última apelación se cerrara. A cuatro años del proceso del 2006, la papelería electoral sigue custodiada \parencite{extra8}. En caso de haberse utilizado urnas electrónicas, estas no podrían haber sido utilizadas para ninguna elección subsecuente (lo que significa que tendría que haberse adquirido o alquilado una segunda infraestructura completa), o renunciar a la posibilidad de averiguar la verdad.

\par 

Pero para el planteamiento anterior partimos de un costo de adquisición cero. La adopción de un sistema electrónico de votación resultando más caro que llevar a cabo una votación tradicional: El Tribunal Supremo de Elecciones de Costa Rica anunció que no implementará urnas electrónicas por su elevado costo  \Parencite{763}. Al hacer este anuncio (y reforzando lo que expondremos en la sección \ref{agilidad-obt-resultados}), el presidente del TSE, Luis Antonio Sobrado, reconoció que hay \emph{algunos riesgos que conlleva la puesta en marcha del voto electrónico como es el tener las urnas en línea, medida que a la fecha ningún país en el mundo ha querido asumir en este tipo de iniciativas}.

\par 

Llegamos entonces a una contradicción: El equipo de votación no es barato, en términos absolutos. Su adquisición por parte de un gobierno o ente de autoridad podría justificarse si se plantea prorratear a lo largo de varias elecciones — pero el equipo tiene que estar sujeto a una estricta vigilancia contínua, incluso en los años en que no será utilizado. Debe recibir mantenimiento, y debe abastecerse con una cantidad no despreciable de insumos, para asegurar un rastro impreso verificado. Además, en caso de sufrir un desperfecto, todas las casillas deben tener un plan de respaldo: Casi indefectiblemente, esto significaría tener papelería tradicional para enfrentar desde un desperfecto del equipo hasta un sabotaje, por ejemplo, en el suministro eléctrico. Por tanto, el supuesto ahorro puede volverse en contra nuestra, convirtiéndose en un gasto mucho mayor al que implican las votaciones tradicionales.









\section{Agilidad en la obtención de resultados}
\label{agilidad-obt-resultados}

\par 

Una de las principales obsesiones de la sociedad actual es la velocidad del acceso a la información. Los medios electrónicos de comunicación y el uso de Internet nos han acostumbrado a que la información debe estar disponible tan pronto ocurren los hechos, y debe llegar a toda la sociedad tan pronto está disponible.



\par 

Los sistemas electorales en general estipulan que, para no manipular los resultados de una elección en proceso, no deben darse a conocer sus resultados parciales hasta que haya cerrado la última de las urnas – No hacerlo de esta manera significaría que el conocimiento público de la tendencia influiría en los resultados de muchas maneras indeseables. Sin embargo, una vez que cierra ésta última urna, en la mayor parte de las democracias modernas hay un periodo típicamente de un par de horas en que es necesario esperar a que las autoridades electorales recopilen la información generada por típicamente decenas de miles de casillas y den a conocer el resultado. Hay una gran presión por parte de los ciudadanos, y muy especialmente de los medios, para que las autoridades electorales publiquen los resultados \emph{de inmediato}. Además del apetito por la información expedita, ésto viene fundamentado en ejemplos de ocultamientos de información que eran realizados conforme los números comenzaban a fluir — Ejemplo de esto son las declaraciones que hizo veinte años más tarde Manuel Bartlett Díaz, quien fuera en 1988 Secretario de Gobernación y presidente de la Comisión Federal Electoral durante las muy cuestionadas elecciones presidenciales de ese año \Parencite{582}: La decisión de no dar a conocer datos preliminares fue tomada por el presidente Miguel de la Madrid, dado que, cito: \emph{si se oficializaba en ese momento –con datos parciales– que Cárdenas Solórzano iba ganando, al final nadie aceptaría un resultado distinto}.



\par 

En la experiencia mexicana, la situación ha cambiado radicalmente de la imperante hace tan sólo dos décadas, como claro resulado de las frecuentes acusaciones de fraude electoral que nuestro sistema electoral ha sufrido — En vez de una demora cercana a una semana, el Instituto Federal Electoral y las autoridades correspondientes de cada uno de las entidades federativas publican los resultados de las \emph{encuestas de salida} y los \emph{conteos rápidos} típicamente dentro de las dos primeras horas tras haber concluído la votación, siempre que haya suficiente márgen estadístico para no causar confusión en la población.



\par 

Impulsar una solución con tantos riesgos como una urna electrónica para ganar como tope estas dos horas sencillamente no tiene sentido. Además, el tiempo invertido por los funcionarios electorales en cada casilla en el conteo de votos emitidos es sólo una fracción del dedicado a las tareas de verificación y protocolización que deben llevarse a cabo antes de declarar concluída una elección. Sumando ésto a que –por consideraciones de seguridad\footnote{Si nos preocupa la falta de seguridad en una computadora que corre aislada de atacantes externos, no tiene sentido siquiera entrar en detalles respecto a la cantidad de riesgos que supondría tenerla conectada a Internet.}– las estaciones de voto no están pensadas para contar con conectividad a red (y que ni los países más industrializados cuentan con una cobertura de Internet del 100\% de su territorio), por lo cual debe haber forzosamente un paso manual de comunicación de resultados al centro de control de la autoridad electoral, el argumento de reducción de tiempos queda descartado.



\par 

Federico Heinz cierra su texto \emph{``¿El voto electrónico mejora la democracia?''}  \Parencite{31} con la siguiente idea:



\begin{quotation}

	Una alternativa factible es realizar la votación mediante formularios que contengan a todos los partidos, dejar que los votantes marquen su elección con tinta, y usar un scanner óptico para hacer un escrutinio automático, verificable mediante un simple recuento manual. No hay nada en contra de un escrutinio electrónico, pero digitalizar el acto mismo de la emisión del voto es extremadamente peligroso para la democracia.

\end{quotation}

\par 

El uso de boletas de papel y tinta aptas para ser scanneadas por equipo de reconocimiento óptico puede ser la opción más adecuada en este sentido. Permite la verificación de cientos de boletas en apenas un par de minutos, y permite conservar todos los atributos positivos del sistema tradicional.

\section{Confiabilidad de los actores}
\label{confiabilidad-actores}

\par 

Algunos proponentes del voto electrónico mencionan que con el voto tradicional en papel siempre hubo fraudes de diversas naturalezas — Robo de paquetes electorales, urnas con más papeletas que electores, papeletas pre-marcadas, voto en cadena, voto repetido con documentos falsos, muertos presentándose masivamente a votar… Todos estos fraudes –y seguramente muchos más– siempre han existido, y que cambiar a una modalidad electrónica no agrava los riesgos — Sin embargo, más que reducir las posibilidades de los agentes fraudulentos, al implementar el voto electrónico estaríamos aumentando la profundidad a la que podrían llegar, y lo que es más importante aún, imposibilitando cualquier acción de auditoría o rendición de cuentas. Si, requiere mayor sofisticación por parte del atacante, que un fraude electoral tradicional, pero le da posibilidad de incidir de una forma mucho más decisiva.



\par 

La votación electrónica tiene muchas modalidades y muchas aristas. En líneas generales, y contrario a lo que muchos esperarían, los expertos en seguridad informática y los activistas sociales involucrados en esta lucha no recomiendan exigir que las urnas electrónicas estén basadas en Software Libre para su funcionamiento, sino que sencillamente recomiendan en contra de su utilización. Citando a Heinz,  \Parencite{31}:



\begin{quotation}

	El mecanismo de auditar completamente el funcionamiento de las urnas es impracticable. Esta es una tarea que sólo podría ser ejecutada por una elite de especialistas, de los que hay muy pocos en el mundo, y requiere la cooperación de las empresas que proveen las urnas así como de todos sus proveedores. Y aún si consiguiéramos todo eso, la eficacia de una auditoría sería más que dudosa: no sólo debemos garantizar que todo el software es correcto (lo que es imposible), sino que además debemos verificar que el software presente en las urnas el día de la elección es idéntico al auditado, tarea que nuevamente requiere de especialistas. ¿Y por qué hemos de confiar en los especialistas, si no queremos confiar en sacerdotes ni en empresas? Una de las muchas virtudes del ``anticuado'' sistema de escrutino tradicional es que cualquier persona que sepa leer, escribir y hacer operaciones de aritmética elemental está en condiciones de controlarlo. Esta es una característica esencial y no debemos renunciar a ella.

\end{quotation}

\par 

Uno de los más interesantes argumentos que ilustran por qué las urnas electrónicas carecen inherentemente de confiabilidad es el presentado —sin aplicarlo en éste ramo específico— por Ken Thompson en 1983  \Parencite{209}, en su discurso al recibir el Premio Turing de la ACM\footnote{Premio al que comunmente se hace referencia como \emph{el Nóbel del cómputo}}. Thompson hace una sencilla demostración de por qué un sistema que llega al \emph{usuario final} (y esto es mucho más cierto hoy en día que en 1983, en que los lenguajes y marcos de desarrollo utilizados suben increíblemente en la escala de la abstracción comparado con lo existente entonces) es prácticamente imposible de auditar por completo un programa, ni siquiera teniendo su código fuente, \emph{ni siquiera teniendo el código fuente del compilador}. Traduciendo de las conclusiones de Thompson:



\begin{quotation}

	La moraleja es obvia. No puedes confiar en el código que no creaste tú mismo. (Especialmente código proveniente de compañías que emplean a gente como yo). No hay un nivel suficiente de verificación o escrutinio de código fuente que te proteja de utilizar código no confiable. En el proceso de demostrar la posibilidad de este tipo de ataque, elegí al compilador de C. Podría haber elegido a cualquier programa que manipule a otros programas, como al ensamblador, cargador, o incluso microcódigo embebido en el hardware. Conforme el nivel de programación se vuelve más bajo, éstos fallos se volverán más y más difíciles de detectar. Esta vulnerabilidad bien instalada en microcódigo será prácticamente imposible de detectar.

\end{quotation}

\par 

Éste argumento ha sido clave para llegar a conclusiones como la adoptada en marzo del 2009 por la Corte Suprema de Alemania:  \Parencite{138}, \Parencite{139}



\begin{quotation}

	Un procedimiento electoral en el que el elector no puede verificar de manera confiable si su voto fue registrado sin falsificación e incluido en el cálculo del resultado de la elección, así como comprender cabalmente de qué manera los votos totales emitidos son asignados y contados, excluye del control público a componentes centrales de la elección, y por lo tanto no alcanza a satisfacer las exigencias constitucionales.

\end{quotation}

\par 

Cabe aquí referir al último punto mencionado en el fragmento citado en la sección \ref{disminucion-costos}: \emph{Un rastro impreso verificado por cada votante}. La única garantía que un votante puede tener de que su voto fue registrado correctamente es que el sistema genere una boleta \emph{impresa y de caracter irrevocable}, misma que sea verificada por el votante al instante, la cual se convertirá en el documento probatorio de la elección\footnote{Y claro está, es fundamental que cada una de estas boletas sea generada por separado, recortada de la inmediata anterior y posterior, con garantía de que no haya un patrón seguible en el corte, para garantizar el anonimato del elector}. No hay manera de que el estado interno de una computadora sea confiable, y mucho menos cuando hablamos del proceso más importante y más sensible de la vida política de un país.

\par 

El punto de la confiabilidad es el que más fervientemente se sigue debatiendo. El caso brasileño resulta muy esperanzador: A diferencia de la mayor parte de los gobiernos de países supuestamente desarrollados, en Brasil la tecnología utilizada para el voto electrónico está completamente basada en tecnología desarrollada localmente, empleando software libre. En noviembre del 2009, el Tribunal Superior Electoral brasileño convocó a la comunidad de seguridad a encontrar vulnerabilidades sobre las estaciones receptoras de votos, a cambio de una recompensa económica para los mejores análisis \Parencite{583}. Dentro de los términos estipulados, sólo uno de los participantes (Sergio Freitas da Silva) logró su propósito  \Parencite{584}. Y si bien no logró vulnerar los resultados de éste sistema, sí logró –mediante un monitoreo de las radiaciones electromagnéticas– averiguar por quién emitía su voto cada uno de los electores, rompiendo el principio de secreto electoral, empleando únicamente equipo casero de bajo costo al buscar que esto fuera meramente una prueba de concepto; un atacante determinado podría utilizar equipo mucho más sofisticado para intervenir las votaciones a mucha mayor distancia.


\par 

Y si bien el sistema empleado por Brasil sale mucho mejor parado que los empleados en Europa y Estados Unidos, no debemos tomar la ausencia de evidencia por evidencia de ausencia: Lo único que demostraron es que ninguno de los atacantes pudo demostrar una vulnerabilidad en el periodo estipulado, o no quiso hacerlo por el precio ofrecido, pero nada indica que no haya fallas no encontradas — O peor aún, \emph{puertas traseras} intencionales.

\section[Votos blancos y nulos]{Votos blancos y nulos: Expresión legítima del ciudadano}

No podemos dejar de llamar la atención a que muchas de las implementaciones llevan a que en la práctica se limite la capacidad de expresión del sentido del voto del ciudadano. A lo largo de la historia, y en diversos países, agrupaciones políticas o grupos de ciudadanos espontáneos han convocado a la anulación del voto como muestra de descontento ante las opciones presentadas, como una expresión de desconfianza ante la clase política toda.

Ejemplos de esta convocatoria podemos verlos en México \parencite{extra1}, donde el nivel de votos nulos alcanzó en las elecciones legislativas de 2009 niveles superiores al 5\% a nivel nacional (equivalente a ser la quinta fuerza política) y en algunas entidades superó el 10\% \parencite{extra2}; en las elecciones legislativas al Parlamento Vasco alcanzó el 8.84\% tras la ilegalización de la izquierda abertzale \parencite{extra5}; en Perú, la localidad de Santiago de Pupuja presentó un 61\% de votos nulos (frente al 8\% del candidato más votado) \parencite{extra6}. Sin embargo, el ejemplo más fuerte viene de la Argentina de mediados de siglo, durante el largo periodo de golpes militares y censura. Ante la prohibición de toda actividad política a Juan Domingo Perón, sus partidarios llamaron al voto en blanco. En las elecciones constituyentes de 1957 hubo cerca del 24\% de votos en blanco \parencite{extra4}. En las elecciones presidenciales de 1963 se repitió el escenario de censura, y los votos en blanco alcanzaron el segundo lugar con 19\% frente al 25\% de Arturo Illia \parencite{extra7}, aunque varios actores de la época sostienen que incluso estas cifras están maquilladas.

Si bien es común escuchar críticas a los impulsores del voto en blanco en el sentido de que no aporta nada o que no tiene influencia en la distribución del \emph{botín} electoral, es derecho de todos los ciudadanos manifestar su descontento de esta manera. En el sistema electoral como el peruano, una alta proporción de votos nulos llevan a la anulación de la votación. En el sistema argentino, se diferencían \emph{votos nulos} (mal realizados, intencionalmente o no; no forman parte de los porcentajes electorales) de \emph{votos blancos} (explícitamente y sin espacio a ambigüedad indicar la no preferencia por ninguno de los candidatos; es reportado dentro de los porcentajes resultantes de la elección). En el sistema mexicano, no se diferencían entre votos nulos y en blanco, lo cual reduce el impacto que éste puede tener. España tiene un sistema similar al mexicano, lo cual motivó la creación de la agrupación \href{http://ciudadanosenblanco.com}{Ciudadanos en blanco}, que se presenta a elecciones con el compromiso de dejar vacíos los escaños parlamentarios que obtengan, y reclamar que el voto en blanco sea (explícitamente) computable.

Independientemente de su validez e impacto legal, voto blanco y nulo son una herramienta de expresión del individuo, y es un atributo electoral que debe ser conservado. En las jurisdicciones donde el sistema electoral contempla los votos en blanco, una urna electrónica presentará esta opción. Sin embargo, donde la ley asume que un voto nulo es un voto \emph{mal emitido}, las autoridades argumentan a favor de la instalación de urnas electrónicas el que éstas imposibilitan emitir votos erróneos. Esto estaría limitando la posibilidad del individuo de mostrar su descontento ante las opciones formales.

\section{Experiencias internacionales}
\label{experiencias-internacionales}

\par 

Por último, presentamos un listado de experiencias en diversos países, ilustrando muy brevemente el tipo de problemas a que puede llevarnos el voto electrónico. Queda claro que esta no es una lista comprehensiva, sólo indicativa. Para una descripción mucho más exhaustiva, sugiero consultar  \Parencite{773}.



\begin{itemize}

	\item En 2004, el Secretario de Estado de California, Kevin Shelley, des-certificó y prohibió el uso de ciertos modelos de urnas electrónicas Diebold en cuatro condados, y ordenó a 10 condados adicionales dar pasos para mejorar la seguridad y confiabilidad de dichos equipos  \Parencite{772}, al descubrirse que el software con que habían sido enviadas dichas urnas no era el mismo que el que se había sometido para certificación.

	\item La elección municipal de 2005 en Montreal, Canadá, se realizó utilizando urnas electrónicas, con resultados desastrosos — Del orden de 45,000 votos fueron contabilizados doblemente  \Parencite{767}. La autoridad electoral realizó un análisis de las elecciones, y publicó un amplio reporte  \Parencite{768} analizando las causas y cursos de acción a seguir, entre los cuales se menciona la necesidad de tener acceso completo al código fuente, el empleo de pruebas de funcionalidad, establecimiento de un plan de respaldo en caso de problemas, la implementación de medidas estrictas para almacenamiento y resguardo de los equipos. Marcel Blanchet, funcionario electoral en jefe, (traducido) \textit{(…) opina que las urnas y terminales de votación electrónicas son tecnologías vulnerables. Más allá, la manera en que fueron manejadas no ofrece suficiente garantía de transparencia y seguridad para asegurar la integridad del voto}.

	\item Ed Felten ha escrito en repetidas ocasiones respecto a lo inadecuadas que son diversas urnas electrónicas. Uno de los primeros ejemplos que publicitó, en 2006, es la pobre seguridad física en dichos equipos — Las urnas pueden ser abiertas por una llave genérica de cajones de oficina y minibares de hotel  \Parencite{765}.

	\item En Argentina, en 2007, se ensayó el voto electrónico en la localidad de Las Grutas, provincia de Río Negro. Hubo una muy gran cantidad de discrepancias entre el padrón electoral y el padrón registrado digitalmente, con lo que muchos votantes no pudieron expresar su voluntad. Mientras en las mesas tradicionales se registró del orden del 70\% de votación, en las mesas con urna electrónica sólo se llegó al 40\%. Además de esto, por errores en el manejo de la urna por parte de las autoridades de una de las casillas, ésta eliminó los registros en vez de guardarlos en la memoria externa.  \Parencite{778}. El ciudadano Sergio Daniel Plos presentó un amparo para que su localidad no volviera a participar en elecciones electrónicas, escrito al cual se adhirieron aproximadamente el 10\% de los votantes de la localidad  \Parencite{779}. En 2010, ante pasos que llevarían a la implantación de voto electrónico en la provincia de Salta, políticos de diversos partidos interpusieron un recurso refiriéndose al caso de Las Grutas  \Parencite{780}.

	\item Felten exhibe también el ejemplo de una estación de votación para las elecciones \textit{primarias} en Nueva Jersey, 2008,  \Parencite{32} en que se puede ver un error \textit{aritmético} al calcular la suma de votos. 

	\item Las elecciones presidenciales de los Estados Unidos de América de noviembre de 2008, si bien no presentaron los graves problemas de legitimidad que sufrieron en 2000 y 2004, presentaron irregularidades en varios Estados. El periódico \textit{Anchorage Daily News} (Alaska) reseña  \Parencite{764} Los siguientes casos: En Virginia y Pensilvania hubo varias descomposturas en las urnas electrónicas, lo que evitó que muchas personas no pudieran ejercer su derecho a voto. En algunos casos, les fueron presentadas \textit{boletas} de tipo erróneo. Por otro lado, en Michigan, el día anterior a la elección se descubrió que varias de las urnas electrónicas presentaban desperfectos, por lo cual se instrumentó una votación tradicional — pero sin papelería electoral específica que presentara las garantías adecuadas de unicidad.

	

\item Las elecciones primarias del Partido Laborista en Israel, 2008, tuvieron que ser pospuestas una vez que habían iniciado, dado que las urnas presentaban problemas de usabilidad — Algunas pantallas no registraban las respuestas de los votantes, otras registraban votos cuando no habían sido aún tocadas, o marcaban opciones equivocadas  \Parencite{777}. Las fallas fueron generalizadas, tanto que el partido tuvo que cancelar la operación y repetirla al día siguiente al estilo \emph{tradicional}, con sobres, papeletas y una urna de cartón.

	\item Después de que Holanda fuera uno de los países pioneros en implementación de urnas electrónicas, el grupo \emph{Wij vertrouwen stemcomputers niet} (\emph{No confiamos en las computadoras votantes}) presentó en vivo, en el programa de televisión EénVandaag, cómo modificar la programación de las urnas electrónicas \emph{Nedap}. Este hecho llevó a un amplio debate, que culminó con un reporte de la Comisión Asesora en Procesos Electorales, recomendando en 2008 revertir la recomendación que llevó a la implementación del voto electrónico, y rechazando la propuesta de reimplementar una nueva generación de urnas electrónicas paliando este problema  \Parencite{774},  \Parencite{775}. Hoy en día, los procesos electorales honadeses son nuevamente en papel, con conteo manual.	\item Todo aparato electrónico emite radiación electromagnética dependiendo de sus procesos internos, mismos que pueden ser \emph{olfateados} por equipos ubicados hasta a decenas de metros. Un ejemplo de esto, hecho completamente con equipo casero, es el ataque resultante de la convocatoria del Tribunal Superior Electoral de Brasil  \Parencite{583} en 2009, por medio del cual, con equipo completamente casero, el atacante logró averiguar el sentido de cada uno de los votos sin acceso al equipo  \Parencite{584},  \Parencite{771}. 



	\item En la India, prácticamente la totalidad de la población vota en urnas exclusivamente electrónicas, desarrolladas por el gobierno nacional en las últimas dos décadas, la EVM. El funcionamiento interno de estos equipos se había mantenido en secreto para evitar que la comunidad dedicada a la seguridad en cómputo encontrara vulnerabilidades; en abril del 2010, un grupo lidereado por Alex Halderman, Hari Prasad y Rop Gonggrijp consiguió una EVM y publicó dos ataques que pueden ser llevados a cabo en unos cuantos minutos, y permiten alterar los resultados. Dado que la EVM no produce rastro en papel y la única evidencia es su estado interno, esta modificación es indetectable, y resulta simple ``obligar'' a estas máquinas a entregar resultados fraudulentos.

\end{itemize}

\par 

Casos hay muchos más. Claro está, hay también importantes casos de éxito — Por volumen poblacional, cabe destacar al total de la población de la India y de Brasil, y aproximadamente la cuarta parte de la población de los Estados Unidos; a pesar de los problemas anteriormente ilustrados, el mero volumen de votaciones efectuadas a través de urnas electrónicas indica que se puede hacer un despliegue masivo. Sin embargo, lo fundamental de los casos anteriores no es el impacto que hayan tenido o que se hayan detectado a tiempo — Lo principal es que nos hacen patentes los problemas de confiabilidad no sólo en los supuestos fundamentos de funcionamiento, sino que en la misma naturaleza humana. Incluso para tareas aparentemente tan simples como la de sumar votos, no hemos podido producir un sólo proceso tan confiable y auditable como la revisión humana de papeletas físicas.
