#!/usr/bin/perl
# ⚠ Esto debe ser UTF8
use utf8;
use DBI;
use YAML;
use File::Temp qw(tempfile tempdir);
use IO::File;
use Getopt::Long;
use Text::Iconv;
use strict;
$|=1;
my ($iconv, %conf, @books, %authors, %sections, $dbh, $html, @parts);

$iconv = Text::Iconv->new('utf8', 'latin1');
%conf = get_config();

web_cookie();

if ($conf{no_db}) {
    # No DB connection, use the known chapters list
    @books = ( {title => 'Software y medios libres',
		chapters => [ { bid => 111,
			        title => 'Software Libre y Construcción ' .
				    'Democrática de la Sociedad',
				authors => [['Gunnar Wolf'],
					    ['Alejandro Miranda']] },
			      { bid => 118,
			        title => 'Esquemas permisivos de ' .
				    'licenciamiento en la creación artística',
				authors => [['Lila Pagola']] },
			      { bid => 122,
			        title => 'Fundamentos teóricos y ubicación ' .
				    'histórica de la economía y sociedad del '.
				    'conocimiento',
				authors => [['Sergio Ordóñez']] }
			     
		    ] },
	       {title => 'Comunidades',
		chapters => [ { bid => 113,
			        title => 'Factores de motivación y elementos '.
				    'de reconocimiento',
				authors => [['Gunnar Wolf']] },
			      { bid => 120,
			        title => 'La construcción colaborativa del ' .
				    'conocimiento visto desde la óptica de ' .
				    'la comunidades de software libre',
				authors => [['Héctor Colina']] }
		    ] },
	       {title => 'Sociedad',
		chapters => [ { bid => 123,
			        title => 'Política 2.0',
				authors => [['Marko Txopitea']] },			      
			      { bid => 245,
				title => 'Analfabetización informática o ' .
				    '¿por qué los programas privativos ' .
				    'fomentan la analfabetización?',
				authors => [['Beatriz Busaniche']] },
			      { bid => 376,
			        title => 'Construyendo metodologías para la ' .
				    'infoinclusión',
				authors => [['Carolina Flores'],
					    ['Erika Valverde']] }
		    ] },
	       {title => 'Apéndices',
		after => '\\appendix',
		chapters => [ { bid => 117,
			        title => 'Educación y Software Libre',
			      	authors => [['Alejandro Miranda']] },
			      { bid => 728,
			        title => 'Sugar',
				authors => [['Werner Westermann']] },
			      { bid => 217,
			        title => 'Voto electrónico',
				authors => [['Gunnar Wolf']] },
			      { bid => 833,
			        title => 'Traducción de herramientas para ' .
				    'revaloración y rescate de la lectura ' .
				    'tradicional',
				authors => [['Antonio Galindo']] },
			      # { bid => 754,
			      #   title => 'La Enseñanza de la Historia en el ' .
			      # 	    'Contexto de la Sociedad del ' .
			      # 	    'Conocimiento. Una propuesta Didáctica de '.
			      # 	    'Aprendizaje Combinado TIC-Aula',
			      # 	authors => [['Víctor Barragán']] }
		    ] } );
} else {
    my ($sth_node, $sth_auth);
    $dbh = DBI->connect("dbi:mysql:dbname=$conf{dbname};host=$conf{dbhost}",
			$conf{dbuser}, $conf{dbpass},
			{mysql_enable_utf8 => 1}) or die;
    $dbh->do("SET NAMES 'utf8'");

    # If we have a DB connection, fetch the list of chapters/authors
    $sth_node = $dbh->prepare('SELECT n.title, ct.field_short_title_value AS ' .
			      'short, pv1.value AS name, pv1.uid, pv2.value ' .
			      'country FROM node n JOIN ' .
			      'content_type_captsec ct USING (nid,vid) JOIN ' .
			      'content_field_autores cfa USING (nid,vid) '.
			      'JOIN profile_values pv1 ON '.
			      'cfa.field_autores_uid=pv1.uid ' .
			      'JOIN profile_values pv2 ON '.
			      'cfa.field_autores_uid=pv2.uid ' .
			      'WHERE nid=? AND pv1.fid=1 AND pv2.fid=4');
    $sth_auth = $dbh->prepare('SELECT pv.value, u.picture FROM ' .
			      'profile_values pv JOIN users u USING (uid) ' .
			      'WHERE pv.fid=6 AND uid=?');

    @books = ();
    @parts = ( { title => 'Software y medios libres',
		 chapters => [111, 118, 122]
	       },
	       { title => 'Comunidades', 
		 chapters => [113, 120]
	       },
	       { title => 'Sociedad',
		 chapters => [123, 245, 376]
	       },
	       { title => 'Apéndices',
		 after => '\\appendix',
		 chapters => [117, 728, 217, 833]  # 754
	       }
	);

    # Known list of nodes for the chapters we will use
    for my $part (0..$#parts) {
	$books[$part] = { title => $parts[$part]{title},
			  after => $parts[$part]{after},
			  chapters => []};
	for my $bid (@{$parts[$part]->{chapters}}) {

	    my (@authors, $title, $short, $auth_text);
	    $sth_node->execute($bid);
	    while (my @row = $sth_node->fetchrow_array) {
		my ($name, $uid, $text, $pict, $pic_file, $ctry);
		($title, $short, $name, $uid, $ctry) = @row;
		push @authors, $name;
		$sth_auth->execute($row[3]);
		($text, $pict) = $sth_auth->fetchrow_array;
		$auth_text = author_snippet($uid, $text);

		if ($pict) {
		    $pic_file = "auth_$uid.png";
		    system('wget', '-nv', '-nc', '--load-cookies', 
			   '/tmp/wget.cookies', '-O', $pic_file,
			   $conf{baseurl} . $pict);
		}
		$authors{$row[2]} = [$auth_text, $pic_file, $ctry];
	    }

	    push( @{$books[$part]{chapters}},
		  { bid => $bid,
		    title => $title,
		    authors => [sort @authors],
		    short => $short} );
	}
    }
}

gen_chapters();
gen_toc();
get_biblio();
log_out();
build_pdf();

sub web_cookie {
    my ($post);
    $post = sprintf('name=%s&pass=%s&op=Iniciar+sesi%%C3%%B3n&' .
		    'form_id=user_login_block',
		    $conf{secouser}, $conf{secopass});

    system(sprintf "wget %s --save-cookies /tmp/wget.cookies " .
	   "--post-data='%s' -nv -O /dev/null", $conf{baseurl}, $post);
}

sub log_out {
    # Say goodbye nicely, avoid piling up tens of logins
    system('wget', '--load-cookies', '/tmp/wget.cookies', 
	   $conf{baseurl} . 'logout');
}

sub get_config {
    my (%conf, $workdir, $destdir, $title, $auth, $h2tex,
	$no_db, $baseurl, $biburl, $skipimg, $dbname, $dbhost,
	$dbuser, $dbpass, $secouser, $secopass);

    GetOptions(
	'workdir=s' => \$workdir, 'destdir=s' => \$destdir, 'title=s' => \$title,
	'authors=s' => \$auth, 'h2tex=s' => \$h2tex, 'no_db' => \$no_db, 
	'baseurl=s' => \$baseurl, 'biburl=s' => \$biburl,
	'skip_images' => \$skipimg, 'dbname=s' => \$dbname, 
	'dbhost=s' => \$dbhost, 'dbuser=s' => \$dbuser, 'dbpass=s' => \$dbpass,
	'secouser=s' => \$secouser, 'secopass=s' => \$secopass
	);

    return (workdir => $workdir   || tempdir(),
	    destdir => $destdir   || '/tmp',
	    title => $title       || "Seminario Construcci\\'on Colaborativa " .
	    "del Conocimiento",
	    auth => $auth         || 'Coordinadores:\\\\ Gunnar Wolf y '  .
	    'Alejandro Miranda',
	    h2tex => $h2tex       || '/home/gwolf/cvs/seco3/gnuhtml2latex',
	    no_db => $no_db       || 0,
	    skipimg => $skipimg   || 0,
	    baseurl => $baseurl   || 'http://seminario.edusol.info/',
	    biburl => $biburl     || 
	        'http://seminario.edusol.info/biblio/export/bibtex/',
	    dbname => $dbname     || 'drupal_conoc',
	    dbhost => $dbhost     || '127.0.0.1',
	    dbuser => $dbuser     || 'drupal_conoc',
	    dbpass => $dbpass     || '',
	    secouser => $secouser || 'gwolf',
	    secopass => $secopass || ''
	);
}

sub gen_toc {
    my (@tex, $file, $fh);
    print " *** Main file (TOC)\n";
    $file = "$conf{workdir}/seco3.tex";
    @tex = ('%% ⚠ Quiero un archivo UTF-8!',
	    '\documentclass[twoside,spanish]{book}', # Aunque me sugieren el KOMA - scrbook
            '\usepackage[T1]{fontenc}',
            '\usepackage[utf8]{inputenc}',
            '\usepackage{babel}',
            '\usepackage[pdftex=true,colorlinks=true,plainpages=false]{hyperref}',
	    '\usepackage{url}',
	    '\usepackage{cclicenses}',
	    '\usepackage{csquotes}',
	    '',
	    '\usepackage[style=authoryear, backref=true, ',
	    '    refsection=part, block=nbpar]{biblatex}',
	    '\ExecuteBibliographyOptions{ autocite=inline, sortcites=true, ',
	    '    labelyear=true, uniquename=true}',
	    '\bibliography{seco3}',
	    '',
	    '\usepackage{graphicx}',
	    '\DeclareGraphicsExtensions{.gif,.png,.jpg}',
	    '',
            '\setcounter{secnumdepth}{2}',
            '\setcounter{tocdepth}{2}',
	    '',
	    '\usepackage{epigraph}',
	    '\usepackage{fancyhdr}',
            '\addto\shorthandsspanish{\spanishdeactivate{~<>.}}', 
	    '',
	    '\usepackage{wrapfig}',
	    '\newcommand{\chapauth}[1]{',
	    '\epigraph {\large \emph{#1}}',
	    '','\vskip 1.5cm',
	    '}',
	    # '\newcommand{\authbox}[1]{',
	    # '  \begin{center}',
	    # '  \begin{minipage}[t]{0.8\columnwidth}',
	    # '     #1',
	    # '  \end{minipage}',
	    # '  \end{center}',
	    # '}',
	    '',
	    '\newcommand{\authordata}[4]{',
	    # Piden en publicaciones que no pongamos la foto - Va sólo
	    # el nombre del autor y su país de origen
	    # '  \begin{minipage}[c]{0.3\linewidth}',
	    # # '    \includegraphics[width=2cm]{#3}',
	    # # Nota que este '%' final tiene significado: Le indica a TeX
	    # # que se pegue, para que no se me rompa un minipage en dos 
	    # # partes... o algo por el estilo :-P
	    # '  \end{minipage}%', 
	    # '  \begin{minipage}[c]{0.7\linewidth}',
	    '    { \large { \bf #1 } — #4 }',
	    # '  \end{minipage}%',
	    '  \nopagebreak',
	    '  \vskip 0.2cm',
	    '  \nopagebreak',
	    '  \noindent',
	    '  #2',
	    '',
	    '  \vskip 0.5cm',
	    '}',
	    '',
	    '\newcommand{\textbox}[1]{',
	    '  \begin{wrapfigure}{r}{5cm}{\scriptsize',
	    '    \rule{5cm}{1pt}',
	    '    #1',
	    '    \rule{5cm}{0.5pt}',
	    '  }',
	    '  \end{wrapfigure}',
	    '}',
	    '',
	    '%% Doble interlineado, para las galeras',
	    '%% \renewcommand{\baselinestretch}{2}',
	    '\renewcommand{\href}[2]{#2\footnote{\url{#1}}}',
	    '',
	    # http://en.wikibooks.org/wiki/LaTeX/Page_Layout
	    # http://aristarco.dnsalias.org/node/21
	    '\pagestyle{fancy}',
	    '\fancyhf{}',
	    '\headheight 38pt',
	    '\fancyhead[LE,RO]{\thepage}',
	    '\fancyhead[LO]{\leftmark}',
	    '\fancyhead[RE]{\rightmark}',
	    '\fancyfoot[LE,RO]{\Large VERSI\\\'ON PRELIMINAR}',
	    '\fancyfoot[RE,LO]{\small \today}',
	    '\renewcommand{\headrulewidth}{0.4pt}',
	    '\renewcommand{\footrulewidth}{0.4pt}',
	    '',
            '\begin{document}',
	    '',
	    '\frontmatter',
	    sprintf('\title{%s}', $conf{title}),
	    sprintf('\author{%s}', $conf{auth}),
	    '\maketitle',
	    licensing(),
	    '\tableofcontents{}',
	    '',
	    '\mainmatter',
	    presentation());


    for my $part (@books) {
	push @tex, '', sprintf('\part{%s}', $part->{title});
	push @tex, $part->{after} if $part->{after};
	for my $chap (@{$part->{chapters}}) {
	print "$chap->{bid} ";
	push(@tex, '',
	     sprintf('\chapter%s{%s}',
		     # Do we have a short title? If so, it goes
		     # within brackets.
		     ($chap->{short} ? "[$chap->{short}]" : ''),
		     $chap->{title}),
	     map({"\\chapauth{$_}"} join("\\\\", @{$chap->{authors}})),
	     "\\input{$chap->{bid}.tex}", '');
	}
	push @tex, '', '\printbibliography[heading=bibintoc]', '';
	print "\n";
    }

    push @tex, '\end{document}';

    $fh = IO::File->new($file, 'w') or die $!;
    $fh->print(join "\n", @tex);
    $fh->close;
}

sub presentation {
    my (@res, $bid, $pres_html);
    @res = ('\chapter{Presentación}');

    # Hard-coded! El 827 es el nodo de la presentación
    $bid = 827;
    $pres_html = "$conf{workdir}/$bid.html";
    if ($conf{no_db} and ! -f $pres_html) {
	if (! -f $pres_html) {
	    print "\n *!* $pres_html does not exist and no DB connection ".
		"was requested!\n";
	}
    }
    book2html($bid, $pres_html) unless $conf{no_db};
    html2tex($pres_html);
    push @res, "\\input{$bid.tex}";

    push @res, '\section{Acerca de los autores}';
    for my $auth (sort {
	my ($fam_a, $fam_b);
	$a =~ /.+ (.+)/;
	$fam_a = $1;
	$b =~ /.+ (.+)/;
	$fam_b = $1;
	$fam_a cmp $fam_b;
		  } keys %authors) {
	push @res, sprintf('\authordata{%s}{%s}{%s}{%s}',
			   $auth,
			   html_fixes($authors{$auth}[0]),
			   $authors{$auth}[1],
			   html_fixes($authors{$auth}[2])
	    );
    }
    
    return @res;
}

sub get_biblio {
    # For some reason I fail to grasp, wget yields an empty bibtex
    # file - But links works...
    my ($fh, $bibtex, @corp_auth);
    $bibtex = `/usr/bin/links --anonymous --source $conf{biburl}`;

    if ($bibtex =~ /^\s*$/) {
	print "* * * Please set the corrresponding cookie to Links for\n" .
	    "      BibTex export. Skipping BibTex download\n";
	if (-f 'seco3.bib') {
	    print "\n (don't worry, you already have it there)\n\n";
	}
	return;
    }

    # Avoid bibtex splitting the names of corporate authors - Surround
    # them with braces. We need the full list of authors for this:
    $bibtex =~ s/author.=.\{(
                   Asamblea.General.de.las.Naciones.Unidas
		 | Colectivo.Politica.en.Red
		 | Comunidad.Virtual.Mistica
		 | Debian.Project
		 | Directeur.général.des.élections.du.Québec
		 | Electronic.Frontier.Foundation
		 | Election.Process.Advisory.Commission
		 | Free.Software.Foundation
		 | Indymedia,.I..M..C.
		 | Junta.de.Extremadura
		 | Open.Source.Initiative
		 | Real.Acad{\'e}mia.Lengua.Espa{\~n}ola
		 | UMIC.-.Ag{\^e}ncia.para.a.Sociedade.do.Conhecimento
		 | U.S..Court.of.Appeals.for.the.Ninth.Circuit
		 | WGBH.Educational.Foundation.\&.Association.for.Computing.Machinery.\(ACM\)
		 | WijVertrouwenStemComputersNiet.nl
                 | Tribunal.Superior.Eleitoral.\(Brasil\)
                 | Banco.Interamericano.de.Desarrollo
                 | Open.Society.Institute
                 )/author = \{\{$1\}/gx;

    my $fh = IO::File->new($conf{workdir} . 'seco3.bib', 'w') or die $!;
    $fh->print($bibtex);
    $fh->close;
}

sub gen_chapters {
    print " *** Individual chapters: ";

    if ($conf{no_db}) {
	print "Not fetching from DB, using locally stored copies " .
	    "($conf{workdir})\n"
    }

    for my $bid (map {map {$_->{bid}} @{$_->{chapters}}} @books) {
	print "$bid ";
	my ($html_file);
	$html_file = "$conf{workdir}/$bid.html";

	if ($conf{no_db} and ! -f $html_file) {
	    if (! -f $html_file) {
		print "\n *!* $html_file does not exist and no DB connection ".
		    "was requested!\n";
	    }
	}
	book2html($bid, $html_file) unless $conf{no_db};
	html2tex($html_file);
    }
    print "\n";
}

sub children_for {
    my ($parent, $book, $sth_hier, $sth_content, $children);
    return undef if $conf{no_db};
    $parent = shift;
    $book = shift;
    $sth_hier = $dbh->prepare('SELECT mlid, plid, nid FROM node JOIN book ' .
			      'USING (nid) JOIN menu_links USING (mlid) ' .
			      'WHERE plid = ? and bid = ? ' .
			      'ORDER BY plid, weight');
    $sth_content = $dbh->prepare('SELECT nr.body, nr.title, ' .
				 'ct.field_short_title_value FROM ' .
				 'node_revisions nr LEFT JOIN ' .
				 'content_type_captsec ct USING (nid, vid) ' .
				 'WHERE nid = ? ORDER BY timestamp ' .
				 'DESC LIMIT 1');
    $children = [];

    $sth_hier->execute($parent, $book);
    while (my ($mlid, $plid, $nid) = $sth_hier->fetchrow_array) {
	my ($body, $title, $short);
	$sth_content->execute($nid);
	($body, $title, $short) = map({html_fixes($_)} 
				      $sth_content->fetchrow_array);
	push @$children, { mlid => $mlid,
			   plid => $plid,
			   nid => $nid,
			   title => $title,
			   short => $short,
			   body => $body,
			   children => children_for($mlid, $book)};

	warn " ** $title → $short" if $short;
	$sections{$title} = $short if $short;
    }

###    print "$parent($book): ", join('→', map {sprintf '%d(%d)', $_->{nid},scalar @{$_->{children}}} @$children),"↵\n";

    return $children;
}

sub book2html {
    my ($bid, $html_file, $nodes, $html_fh);
    $bid = shift;
    $html_file = shift;
    $nodes = children_for(0, $bid);

    $html_fh = IO::File->new($html_file, 'w') or die $!;
    $html_fh->print('<html><head></head><body>'.
		    html_join($nodes),
		    '</body></html>');
    $html_fh->close;    
}

sub html2tex {
    my ($html_file, $tex_file, $bid, $conv, $tex_fh);
    $html_file = shift;

    $tex_file = $html_file;
    $tex_file =~ s/html$/tex/;
    $tex_fh = IO::File->new($tex_file, 'w') or die $!;

    $conv = sprintf(($conf{skipimg}?
		     '%s -n -H -s -P %s' : 
		     '%s -n -H -s -P -g %s' 
		    ), $conf{h2tex}, $html_file);
    $tex_fh->print(intermediate_tex_fixes(`$conv`));
}

sub html_join {
    my ($tree, $depth, $res);
    $tree = shift;
    $depth = shift || 0;

    $res = '';
    for my $item (@$tree) {
	if ($depth) {
	    my $text = $item->{title} || $item->{short};
	    # First level ($depth==0) is already set as the chapter
	    # name
	    $res .= "\n<h$depth>$text</h$depth>\n";
	}
	$res .= "$item->{body}\n\n";
	$res .= html_join($item->{children}, $depth+1);
    }

    return $res;
}

sub html_fixes {
    my ($txt);
    $txt = shift;

    # When we indented via tinymce, instead of a <blockquote>, we got
    # a nasty <p style="padding-left:30px;"> - The horror!
    $txt =~ s!<p style="padding-left:30px;">(.*)</p>!<blockquote>$1</blockquote>!g;
    $txt =~ s!<p[^>]+>!\n\n!g;
    $txt =~ s!<br />!!g;
    $txt =~ s!</p>!!g;
    $txt =~ s!&nbsp;!!g;
    $txt =~ s!\r!!g;

    return $txt;
}

sub intermediate_tex_fixes {
    my (@in, @out);
    @in = @_;
    @out = ();

    for my $lin (@in) {
	# References: We will leave them to BibTeX. However, we have
	# to capture and mangle each instance, so use a loop instead
	# of a greedy regex
	#
	# http://en.wikipedia.org/wiki/BibTeX
	# http://www.cs.stir.ac.uk/~kjt/software/latex/showbst.html
	1 while $lin =~ s!\[bib\]([^[]*)\[/bib\]! \\Parencite{$1}!;

	# Footnotes. We avoid over-reaching by disallowing an extra
	# '[' within a footnote... hope it won't mess with whatnot :-/
	$lin =~ s!\[fn\]([^[]*)\[/fn\]!\\footnote{$1}!g;

	# # Don't number subsubsections or lower level divisions
	# $lin =~ s!\\(subsubsection|(?:sub)?paragraph)!\\$1*!;

	# For mere cleanness, ignore lines which are completely
	# comments (i.e. gnuhtml2latex's banner)
	next if $lin =~ /^\s*%/;

	# Unescape \TeX and \LaTeX's backslashes
	$lin =~ s/\$\\backslash\$((?:La)?TeX)/\\$1/g;

	# If we have short headings for sections/subsections, here is
	# the place to mix them in
	if ( $lin =~ /^(.\w*section)\{(.*)\}/) {
	    my ($level, $full, $short);
	    $level = $1;
	    $full = $2;

	    if ($short = $sections{$full}) {
		$lin = sprintf "%s[%s]{%s}\n", $level, $short, $full;
	    }

	    # die "* * * $lin\n      $full\n\n" . join("\n  + ", keys %sections)
	    # 	if $lin =~ /avant/;
	}

	# Quotes are problematic as they often affect the immediate
	# character. I still have to balance them (i.e. make them
	# open/close instead of just using ''), but at least this
	# works for now
	$lin =~ s/"/''/g;

	push @out, $lin;
    }

    return join("\n", @out);
}

sub build_pdf {
    chdir $conf{workdir};
    system 'pdflatex', 'seco3';
    system 'for i in seco3*.aux; do bibtex $i; done';
    system 'pdflatex', 'seco3';
    system 'pdflatex', 'seco3';
    link("$conf{workdir}/seco3.pdf", "$conf{destdir}/seco3.pdf") unless
	$conf{destdir} eq $conf{workdir};
}

sub licensing {
    q[\thispagestyle{empty}

\vfill
\begin{center}
  {\Huge !`Copia este libro!}
\end{center}

\vskip 1cm

\noindent Los textos que componen este libro se publican bajo formas de
licenciamiento que permiten la copia, redistribuci\'on y la realizaci\'on
de obras derivadas siempre y cuando \'estas se distribuyan bajo las
mismas licencias libres y se cite la fuente.

\vskip 0.5cm

\noindent El copyright de los textos individuales es de los respectivos autores.

\vskip 0.5cm

\begin{center}
  {\Huge Compartir no es delito.}
\end{center}

\vfill

\noindent El presente trabajo est\'a licenciado bajo un esquema Creative Commons
Reconocimiento -- Compartir bajo la misma licencia ({\tt CC-BY-SA}) 3.0 Unported

\medskip

\begin{center}
  {\Huge \cc \bysa }

  {\small \url{http://creativecommons.org/licenses/by-sa/3.0/deed.es_GT}}
\end{center}

\vfill

\begin{wrapfigure}{l}{9cm}
  { \scriptsize
    \rule{9cm}{1pt}
    Seminario Construcci\'on Colaborativa del Conocimiento\\\\
    (Datos de los autores)\\\\
    (Datos de la editorial)\\\\
    (Datos de la obra)\\\\
    ISBN 000-000-00000-0-0\\\\
    \rule{9cm}{1pt}
  }
\end{wrapfigure}

]
}

sub author_snippet {
    my ($text, $uid, $filename, $fh, $res);
    $uid = shift;
    $text = shift;

    # Dump the author info on a temporary file, convert it to TeX, and
    # hand back the TeX filename
    # $file_src = sprintf '%s/auth_%s.html', $conf{workdir}, $uid;
    # $file_tex =~ s/html$/tex/;

    ($fh, $filename) = tempfile();
    # $fh = IO::File->new($file_src, 'w') or die $!;
    $fh->print('<html><body>'); # to keep gnuhtml2latex happy...
    $fh->print($iconv->convert($text));
    $fh->close;

    $res = intermediate_tex_fixes(`$conf{h2tex} -n -H -s -P $filename`);

    unlink($filename);
    return $res;
}
